package mesus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidatorContext;

import org.junit.Before;
import org.junit.Test;

import atg.taglib.json.util.JSONObject;
import pap.mesus.controller.validator.CEP.CEPValidator;

public class CEPValidatorTest extends CEPValidator {
	private List<String> ceps;
	private CEPValidator cepValidator;
	@org.mockito.Mock
	ConstraintValidatorContext constraintValidatorContext;

	@SuppressWarnings("serial")
	@Before
	public void setUp() throws Exception {
		cepValidator = new CEPValidator();
		ceps = new ArrayList<String>() {
			{
				add("80730-460");
				add("80730460");
				add("80.730-460");
				add("80730-46");
				add("80.730-46");
				add("80.730460");
				add("");
			}
		};
	}

	@Test
	public void testIsValid() {
		assertTrue(cepValidator.isValid(ceps.get(0), constraintValidatorContext));
		assertFalse(cepValidator.isValid(ceps.get(1), constraintValidatorContext));
		assertTrue(cepValidator.isValid(ceps.get(2), constraintValidatorContext));
		assertFalse(cepValidator.isValid(ceps.get(3), constraintValidatorContext));
		assertFalse(cepValidator.isValid(ceps.get(4), constraintValidatorContext));
		assertFalse(cepValidator.isValid(ceps.get(5), constraintValidatorContext));
		assertFalse(cepValidator.isValid(ceps.get(6), constraintValidatorContext));
	}

	@Test
	public void testPesquisa() {
		JSONObject j = CEPValidator.pesquisa("80730-460");
		assertFalse(j.isNull(JSON_CEP));
		j = CEPValidator.pesquisa("80730");
		assertTrue(j.isNull(JSON_CEP));
	}

}
