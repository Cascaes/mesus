<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><%@ page contentType="text/html; charset=UTF-8"%><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Nada de prato vazio!</title>
	<spring:url value="/resources/imagens/rotten-tomato.jpg" var="tomate" />
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css' />
	<style>
	
	body {
		margin: 0;
		font-family: 'Ubuntu', sans-serif;
		position: absolute;
		width: 800px;
		top: 50px;
		margin-left: -400px;
		left: 50%;
	}
	img, div {
		float: left;
		
	}
	img {
		margin: 100px 20px;
		
	}
	h2 {
		width: 300px;
		text-align: justify;
	}
	h1 {
		font-size: 100px;
		color: #F00;
		text-shadow: -3px -3px 4px rgba(255, 0, 0, 1);
	}
	</style>
 </head>
   
 <body>
   <img alt="Erro!" src="${tomate}" />
   <div>
   	<h1>Ooops!</h1>
   	<div>Você não possui permissão para acessar esta página! Retornando à página principal em <span>10</span> segundo<span>s</span></div>
   </div>
 </body>
 </html>