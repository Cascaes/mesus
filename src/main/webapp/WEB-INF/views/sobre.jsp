<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Nada de prato vazio!</title>
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/imagens/loading_principal.gif" var="carregando" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/imagens/fundo_principal/fome_brasil3.jpg" var="fundoBonito" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/imagens/favicon" var="favicon" />

<jsp:include page="favicons.jsp" />


<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<script src="${jQuery}"> </script>
<style>
body {
	background-image: url(${fundoBonito});
	background-size: cover;
	background-attachment: fixed;
}

p {
	width: 600px;
	font-family: 'Ubuntu', sans-serif;
	font-size: 20px;
	font-weight: 600;
	text-align: justify;
	position: absolute;
	background: rgba(255, 255, 255, 0.8);
	padding: 20px;
	word-spacing: 5px;
	margin-bottom: 30px;
	letter-spacing: 2px;
	color: #333;
	box-shadow: 2px 2px 5px 0px rgba(255, 255, 255, 0.75);
	border-radius: 10px; left : 50%;
	margin-left: -300px;
	left: 50%;
}
header {
	position: fixed;
}
</style>
</head>

<body>
	<header>
		<nav>
			<div id="cabecalhoParteEsquerda">
				<c:url var="index" value="/" />
				<a href="${index }" style="border: none;"><img src="${logotipo}"
					alt="Mesus" /></a>
			</div>
		</nav>
	</header>
	<section>
		<p>Mais do que simplesmente um site ou um aplicativo, a ideologia do Mesus surgiu ao enxergarmos a necessidade
de encurtarmos o caminho para que alimentos hoje desperdiçados pudessem ser aproveitados e chegar até àqueles
que hoje vivem num contexto de privações e fome.
Percebemos que existem várias iniciativas de redistribuição de alimentos, de empresas des destaque e tantos outros projetos de associações
pequenas que não possuem grande destaque, como as doações de sopas noturnas.
Com tantas iniciativas, percebemos que um dispositivo virtual poderia ajudar os projetos já existentes a mapear a melhor rota entre doador e receptor e também aumentar o número de doadores e receptores com o uso de um dispositivo como o Mesus.
<br /><br />
Nosso sonho maior é acabar com a cultura do desperdício no Brasil, gerando mais economia para empresas e instituições, mas também de fomentar
a cultura da generosidade, para que possamos também atuar fortemente no combate á fome e desnutrição no nosso país.</p>
	</section>
</body>
</html>
