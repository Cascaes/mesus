<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@taglib
	uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/css/style_cadastro2.css"
	var="formatarForms" />
<spring:url value="/resources/imagens/loading.gif" var="loading" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/js/mask.js" var="mask" />
<spring:url value="/resources/ps" var="perfectscrollbar" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${formatarForms}" rel="stylesheet" />
<link href="${painelCss}" rel="stylesheet" />
<style>
body, html {
	height: 500px;
}
body {
	position: absolute;
	left: 50%;
	margin-left: -250px;
	
}
#carr {
	display: none;
	vertical-align: middle;
}
</style>
<script src="${jQuery}"> </script>
<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
<script src="${mask}"> </script>
<script>

function checkboxesMarcados() {
	var completo = 0;
	if ($('#manipulado').is(':checked'))
		completo++;
	if ($('#inteiro').is(':checked'))
		completo++;
	if ($('#incluso').is(':checked'))
		completo++;
	return completo;
}
function preencherBotao(porcento) {
	$('#botSalvar').css(
			'background',
			'-webkit-linear-gradient(left, #373737 ' + porcento
					+ '%, #DFDFDF ' + porcento + '%)');
	$('#botSalvar').css(
			'background',
			'-moz-linear-gradient(left, #373737 ' + porcento
					+ '%, #DFDFDF ' + porcento + '%)');
	$('#botSalvar').css(
			'background',
			'-ms-linear-gradient(left, #373737 ' + porcento + '%, #DFDFDF '
					+ porcento + '%)');
	$('#botSalvar').css(
			'background',
			'linear-gradient(left, #373737 ' + porcento + '%, #DFDFDF '
					+ porcento + '%)');
}
$(document).ready(function() {
	var gramas = $('#gramas'), grupoAlimentarId = $('#grupoAlimentarId');
	gramas.on('keyUp', function() {
		$(this).val($(this).val().replace(/[^\d]/g, ''));
	});
	$('#validade').mask('00/00/0000');
	$('#botSalvar').click(function() {
		if (checkboxesMarcados() === 3){
			window.parent.opcoes.removeClass('marcado').removeClass('marcadoTexto');
			window.parent.$('[data-link=listaDoacoes]').addClass('marcadoTexto');
			if($('[name=unidadePeso]:checked').val() == 'kg') {
				$('#g').trigger('click');
			}
			
			window.parent.$('#carregando').show();
			window.parent.$('#logotipo').hide();
			$('#cadastroDoacao').submit();
		}
	});
	$('#botCancelar').click(function() {
		window.location.href='listaDoacoes';
	});
	$('input[type=checkbox]').change(function() {
		var tot = checkboxesMarcados();

		if (tot === 0)
			preencherBotao(0);
		else if (tot === 1)
			preencherBotao(33);
		else if (tot === 2)
			preencherBotao(66);
		else
			preencherBotao(100);

	});
	var erros = $('#erros');
	if ($('#grupoErros').children('div').length > 0) {
		erros.fadeIn();
	}
	$('#fecharErro').click(function() {
		erros.fadeOut();
	});
	$('[name=unidadePeso]').change(function() {
		var g = gramas.val();
		if($('[name=unidadePeso]:checked').val() == 'g') {
			g = g.replace(/,/, ".");
			g *= 1000;
		}
		else {
			g /= 1000;
			g = g.toString().replace(/\./, ",");
		}
		g = g == 0 ? '' : g
		gramas.val(g);
	})
	$('#enquadrados').click(function() {
		$.ajax({
			url : 'enquadrado',
			type : 'GET',
			data : {
				id : $('#grupoAlimentarId').val()
			},
			beforeSend: function() {
				$('#carr').fadeIn();
				window.parent.$('#carregando').show();
				window.parent.$('#logotipo').hide();
				$('#enquadrados').hide();
			},
			dataType : 'html',
			success : function(dados) {
				$('#alimentosEnquadrados').html(dados);
				$('#alimentosEnquadrados').fadeIn();
				$('#alimentosEnquadrados i').on('click', function() {
					$('#alimentosEnquadrados').fadeOut();
				});
				$('#alimentosEnquadrados ol').perfectScrollbar();
				window.parent.$('#carregando').hide();
				window.parent.$('#logotipo').show();
				$('#carr').fadeOut();
				$('#enquadrados').show();
			}
		});
	});
	window.parent.$('#carregando').hide();
	window.parent.$('#logotipo').show();
});
</script>
</head>

<body>

	<div id="alimentosEnquadrados"></div>

	<c:url var="cD" value="/area/crud_doacao?edit=${crudDoacao.edit }" />
	<form:form method="POST" modelAttribute="crudDoacao" action="${cD}"
		id="cadastroDoacao">
		<div id="erros">
			<div style="text-align: right">
				<img id="fecharErro" src="${fechar}" alt="fechar" />
			</div>
			<div id="grupoErros">
				<form:errors path="gramas" element="div" />
			</div>
		</div>
		<div>
			<div class="rotulo">Selecione o grupo alimentar*</div>
			<div class="primeirosDados">
				<form:select path="grupoAlimentarId">
					<form:options items="${grupoAlimentar}" itemValue="id"
						itemLabel="nome" />
				</form:select>
			</div>
		</div>
		<div>
			<div class="rotulo">
				Peso* <input type="radio" id="kg" checked="checked" name="unidadePeso"
					value="kg" /><span class="primeirosDados"> quilogramas (Kg)</span>
				<input type="radio" id="g" name="unidadePeso" value="g" /><span
					class="primeirosDados"> gramas (g)</span>
			</div>
			<div class="primeirosDados">

				<div>
					<form:input path="gramas" />
				</div>
			</div>
		</div>
		<div>
			<div class="primeirosDados">
				<input type="checkbox" id="manipulado" /> Compromento-me que os
				alimentos <b>NÃO</b> estão manipulados ou processados.
			</div>
		</div>
		<div>
			<div class="primeirosDados">
				<input type="checkbox" id="inteiro" /> Compromento-me que os
				alimentos estão inteiros.
			</div>
		</div>
		<div>
			<div class="primeirosDados">
				<input type="checkbox" id="incluso" />
				<div style="display: inline-block; vertical-align: middle;">
					Compromento-me que os alimentos se enquadram no grupo alimentar
					selecionado.<br /> <span id="enquadrados">Ver alimentos enquadrados </span> <img id="carr" src="${loading}" />
				</div>
			</div>
		</div>
		<div>
			<div class="primeirosDados">
				<div id="botSalvar">Salvar</div>
			</div>
			<c:if test="${crudDoacao.edit }">
				<div class="primeirosDados">
					<div id="botCancelar">Cancelar</div>
				</div>
			</c:if>
		</div>
		
	</form:form>
</body>
</html>