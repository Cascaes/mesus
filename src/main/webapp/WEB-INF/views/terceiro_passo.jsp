<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><%@ page contentType="text/html; charset=UTF-8"%><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Nada de prato vazio!</title>
	<spring:url value="/resources/css/style.css" var="coreCss" />
	<spring:url value="/resources/css/style_cadastro.css" var="cadastroCss" />
	<spring:url value="/resources/css/style_cadastro2.css" var="segundoPassoCss" />
	<spring:url value="/resources/css/style_cadastro3.css" var="terceiroPassoCss" />
	<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
	<spring:url value="/resources/termosdeuso.txt" var="tdu" />
	<spring:url value="/resources/imagens/fechar.png" var="fechar" />
	<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
	<spring:url value="/resources/js/mask.js" var="mask" />
	<spring:url value="/resources/ps" var="perfectscrollbar" />
	<spring:url value="/resources/imagens/favicon" var="favicon" />
	<c:set var="termosDeUso" value="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${tdu}" />
	
	<jsp:include page="favicons.jsp" />
	
	
	<link rel="stylesheet" href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="${coreCss}" rel="stylesheet" />
	<link href="${cadastroCss}" rel="stylesheet" />
	<link href="${segundoPassoCss}" rel="stylesheet" />
	<link href="${terceiroPassoCss}" rel="stylesheet" />
  	<script src="${jQuery}"> </script>
  	<script src="${mask}"> </script>
  	<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
  	<script>
  	
  	$(document).ready(function() {
  		
  		function resultadoRepet() {
  	  		if(senha.val().trim() || repetirsenha.val().trim()) {
  				if(senha.val() == repetirsenha.val())
  					repet.removeClass('bateuNao').addClass('bateuSim');
  				else
  					repet.removeClass('bateuSim').addClass('bateuNao');
  			}
  	  	}
  	  	function resultadoRegex(ref, resultado) {
  	  		resultado ? ref.removeClass('bateuNao').addClass('bateuSim') : ref.removeClass('bateuSim').addClass('bateuNao');
  	  	}
  	  	function deuNegativo(ref) {
  	  		ref.removeClass('bateuSim').addClass('bateuNao');
  	  	}
  	  	function qualExatamentePressionado(event) {
  	  		event.data.alvo.removeClass('corNormalMais').addClass('corRelacaoMais');
  		}
  	  	function qualExatamenteSoltado(event) {
  	  		event.data.alvo.removeClass('corRelacaoMais').addClass('corNormalMais');
  		}
  	  	function relacionarQuandoSobre(event) {
  	  		event.data.alvo.removeClass('corNormal').addClass('corRelacao');
  		}
  	  	function relacionarQuandoFora(event) {
  	  		event.data.alvo.removeClass('corRelacao').removeClass('corNormalMais').addClass('corNormal');
  		}
  		function mostraBotaoSalvar(ref) {
  			var campoSenha = minusc.hasClass('bateuSim') && maiusc.hasClass('bateuSim') && temNum.hasClass('bateuSim') && maior6.hasClass('bateuSim') && repet.hasClass('bateuSim');
  			var campoLiSim = campoSenha &&  aceito.hasClass('bateuSim');
  			if(campoLiSim) {
				analiseSenha.fadeOut('fast', function() {
					botSalvar.fadeIn('fast');
				});
			} else {
				botSalvar.fadeOut('fast', function() {
					analiseSenha.fadeIn('fast');
				});
				
			}
			if(campoSenha) {
				senha.removeClass('fundoErro').addClass('fundoCerto');
				repetirsenha.removeClass('fundoErro').addClass('fundoCerto');
  			} else if(ref != null) {
				ref.removeClass('fundoCerto').addClass('fundoErro');
  			}
			
  		}
  		
  		var senha = $('#senha'), 
	  	    repetirsenha = $('#repetirsenha'),
	  	    minusc = $('#carMin'),
	 	    maiusc = $('#carMai'),
	 	    temNum = $('#temNum');
  			maior6 = $('#maior6'),
  			aceito = $('#aceito'),
  			erros = $('#erros'),
  			destmin6 = $('#destmin6'),
  			destmaius = $('#destmaius'),
  			destminus = $('#destminus'),
  			destnum = $('#destnum'),
  			destrep = $('#destrep'),
  			repet = $('#repet'),
  			botSalvar = $('#botSalvar'),
  			analiseSenha = $('#analiseSenha'),
  			termosUso = $('#termosUso'),
  			liSim = $('#liSim');
  			
  			
  		$('#botSalvar').click(function() {
  			$('#cadastroFinal').submit();
  		});
  		
  		var blocoValidacao = [
			{flag : minusc, destiny: destminus},
			{flag : maiusc, destiny: destmaius},
			{flag: temNum, destiny: destnum},
			{flag : maior6, destiny: destmin6},
			{flag : repet, destiny: destrep},
			{flag : aceito, destiny: termosUso}
 		];
  		for(var ref = 0; ref < blocoValidacao.length; ++ref) {
  			blocoValidacao[ref].flag.mousedown({alvo: blocoValidacao[ref].destiny}, qualExatamentePressionado);
  			blocoValidacao[ref].flag.mouseup({alvo: blocoValidacao[ref].destiny}, qualExatamenteSoltado);
  			blocoValidacao[ref].flag.mouseover({alvo: blocoValidacao[ref].destiny}, relacionarQuandoSobre);
  			blocoValidacao[ref].flag.mouseout({alvo: blocoValidacao[ref].destiny}, relacionarQuandoFora);
  		}
  		liSim.change(function() {
  			
  			liSim.is(":checked") ? aceito.removeClass('bateuNao').addClass('bateuSim') : aceito.removeClass('bateuSim').addClass('bateuNao');
  			mostraBotaoSalvar();
		});
  		senha.keyup(function() {
			var v = $(this).val();
			resultadoRegex(minusc, /[a-z]/.test(v));
			resultadoRegex(maiusc, /[A-Z]/.test(v));
			resultadoRegex(temNum, /[0-9]/.test(v));
			resultadoRegex(maior6, /.{6,}/.test(v));
			resultadoRepet();
			
			mostraBotaoSalvar(senha);
			if(!senha.val().trim()) 
				senha.removeClass('fundoCerto').removeClass('fundoErro').addClass('fundoBranco');
  			else
  				senha.removeClass('fundoBranco');
				
		});
  		repetirsenha.keyup(function() {
  			resultadoRepet();
  			mostraBotaoSalvar(repetirsenha);
  			if(!repetirsenha.val().trim()) 
  				repetirsenha.removeClass('fundoCerto').removeClass('fundoErro').addClass('fundoBranco');
  			else
  				repetirsenha.removeClass('fundoBranco');
  			 
  		});
  		if($('#grupoErros').children('div').length > 0){
  			erros.animate({
  				bottom: 0
  			}, 600);
  		}
  		$('#fecharErro').click(function() {
  			erros.animate({
  				bottom: '-1000px'
  			}, 600);
  		});
  		$('#lerTermos').click(function() {
			$('#termosDeUso').fadeIn('fast');
		});
  		$('#fecharTermos').click(function() {
			$('#termosDeUso').fadeOut('fast');
		});
  		
  	});
  	
  	</script>
 </head>
   
 <body>
   <pre id="termosDeUso">
  	
   <div>
    <div id="fecharTermos"><span class="material-icons">close</span></div>
  	  <p><c:import url="${termosDeUso}" /></p>
    </div>
   </pre>
   <header>
   	<nav>
   		<div id="cabecalhoParteEsquerda">
   			<c:url var="index" value="/" />
				<a href="${index }" style="border: none;"><img src="${logotipo}"
					alt="Mesus" /></a>
   		</div>
   		<c:url var="irpainel" value="/area/principal" />
   		<div id="cabecalhoParteDireita">
   			<div><a href="sobre">Sobre</a></div>
   			<div><a href="pr">Perguntas &amp; Respostas</a></div>
   			<div><a href="${irpainel}">Entrar</a></div>
   		</div>
   	</nav>
   </header>
   <section>
	   <div id="faixaCentral">
	   		<div id="passos">
	   		<div class="numeroPasso">1</div>
	   		<div class="traco">____________________</div>
	   		<div class="numeroPasso">2</div>
	   		<div class="traco">____________________</div>
	   		<div class="numeroPasso numeroPassoPosicaoAtual">3</div>
	   	</div>
	</div>
   </section>
   <form:form method="POST" modelAttribute="terceiroPasso" action="cadastro_senha" id="cadastroFinal">
   		<p>O e-mail <span class="destaque">${primeiroPasso.email }</span> será usado como login. 
   		Crie sua senha abaixo, e <span id="destrep" class="corNormal">repita posteriormente para assegurar</span>. 
   		A senha deverá ter no <span id="destmin6" class="corNormal">mínimo 6 caracteres</span>, 
   		<span id="destmaius" class="corNormal">pelo menos algum maiúsculo</span>, 
   		<span id="destminus" class="corNormal">pelo menos algum minúsculo</span> e <span id="destnum" class="corNormal">pelo menos algum número</span>.
   		</p>
		<div>
			<div class="rotulo">Senha*</div>
			<div class="primeirosDados">
				<form:password path="senha" cssClass="fundoBranco" />
			</div>
		</div>
		<div>
			<div class="rotulo">Repetir senha*</div>
			<div class="primeirosDados">
				<form:password path="repetirsenha" cssClass="fundoBranco" />
			</div>
		</div>
		<div>
			<div id="termosUso">
				<div><input type="checkbox" id="liSim"></div>
				<div id="lerTermos">Ao clicar o botão salvar, você concorda com os termos de uso do Mesus.</div>
			</div>
		</div>
		<div id="erros">
			<div style="text-align: right"><img id="fecharErro" src="${fechar}" alt="fechar" /></div>
			<div id="grupoErros">
				<form:errors path="senha" element="div" />
				<form:errors path="repetirsenha" element="div" />
				
			</div>
		</div>
		<div>
			<div class="primeirosDados">
				<div id="botSalvar" class="botao">Salvar</div>
				<div id="analiseSenha">
					<div id="carMin" class="bateuNao">a</div>
					<div id="carMai">A</div>
					<div id="temNum">0</div>
					<div id="maior6">6</div>
					<div id="repet">=</div>
					<div id="aceito" class="material-icons">check</div>
				</div>
			</div>
		</div>
	</form:form>
    
  
 </body>
 </html>
