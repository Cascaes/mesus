<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@taglib
	uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/css/style_cadastro2.css"
	var="formatarForms" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/js/mask.js" var="mask" />
<spring:url value="/resources/ps" var="perfectscrollbar" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${formatarForms}" rel="stylesheet" />
<link href="${painelCss}" rel="stylesheet" />
<style>
body, html {
	height: 500px;
}

td i {
	font-size: 14px !important;
	cursor: default;
	color: #F70 !important;
	transition: all 0.7s ease;
}

table {
	border-collapse: collapse;
	margin-left: auto;
	margin-right: auto;
}

td i:hover {
	color: #0F0 !important;
}

th {
	background-color: #FA0;
	color: #FFF;
}

tr:first-child, tr:nth-child(even) {
	box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75);
}

tr:nth-child(even) {
	background-color: #FDD;
}

th, td {
	padding: 10px;
	font-size: 14px;
}

td {
	text-align: center;
}

</style>
<script src="${jQuery}"> </script>
<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
<script src="${mask}"> </script>
<script>
	var id;
	$(document).ready(function() {
		
		$('[data-cancel]').click(function() {
			$('#cancela').fadeIn();
			id = $(this).data('cancel');
		});
		$('[data-pronto]').click(function() {
			$('#conclui').fadeIn();
			id = $(this).data('pronto');
		});
		$('#cancelaNao').click(function() {
			$('#cancela').fadeOut();
			id = null;
		});
		$('#cancelaSim').click(function() {
			$('#cancela').fadeOut();
			window.location.href='cancelarReserva?n=' + id;
			window.parent.$('#carregando').show();
			window.parent.$('#logotipo').hide();
		});
		
		$('#concluiNao').click(function() {
			$('#conclui').fadeOut();
			id = null;
		});
		$('#concluiSim').click(function() {
			$('#conclui').fadeOut();
			window.location.href='confirmarReserva?n=' + id;
			window.parent.$('#carregando').show();
			window.parent.$('#logotipo').hide();
		});
		window.parent.$('#carregando').hide();
		window.parent.$('#logotipo').show();
	});
</script>
</head>

<body>
	<div id="cancela" class="popupDecisao">
		<div>Tem certeza que deseja cancelar a reserva?</div>
		<div>
			<div id="cancelaSim" class="popupSim">Sim</div>
			<div id="cancelaNao" class="popupNao">Não</div>
		</div>
	</div>
	<div id="conclui" class="popupDecisao">
		<div>Confirma que irá buscar a reserva?</div>
		<div>
			<div id="concluiSim" class="popupSim">Sim</div>
			<div id="concluiNao" class="popupNao">Não</div>
		</div>
	</div>
	
	<table>
		<tr>
			<th>Nome</th>
			<th>E-mail</th>
			<th>Identificador</th>
			<th>Grupo Alimentar</th>
			<th>Peso (g)</th>
			<th>Situação</th>
			<th colspan="2">Ações</th>
		</tr>
		<c:forEach items="${reservas }" var="reserva">
			<c:choose>
				<c:when test="${not reserva.rejeitada }">
					<tr>
						<td style="white-space: nowrap;">${reserva.doacao.usuario.primeiroNome() }</td>
						<td>${reserva.doacao.usuario.email }</td>
						<td>${reserva.doacao.identificador }</td>
						<td>${reserva.doacao.grupoAlimentar.nome }</td>
						<td>${reserva.doacao.gramas }</td>
						<td>${reserva.doacao.status.titulo }</td>
						<c:choose>
							<c:when test="${reserva.doacao.status.rotulo eq 'PROCESSO' }">
								<td><i class="material-icons" data-pronto="${reserva.id}">directions_car</i></td>
								<td><i class="material-icons" data-cancel="${reserva.id}">cancel</i></td>
							</c:when>
							<c:otherwise>
								<td colspan="2"></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:when>
			</c:choose>
		</c:forEach>
	</table>
</body>
</html>