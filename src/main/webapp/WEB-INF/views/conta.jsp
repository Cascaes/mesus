<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><%@ page contentType="text/html; charset=UTF-8"%><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Nada de prato vazio!</title>
	<spring:url value="/resources/css/style.css" var="coreCss" />
	<spring:url value="/resources/css/style_cadastro.css" var="cadastroCss" />
	<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
	<spring:url value="/resources/imagens/fechar.png" var="fechar" />
	<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
	<spring:url value="/resources/js/mask.js" var="mask" />
	<spring:url value="/resources/ps" var="perfectscrollbar" />
	<spring:url value="/resources/imagens/favicon" var="favicon" />
	
	
	<jsp:include page="favicons.jsp" />
	
	<link rel="stylesheet" href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="${coreCss}" rel="stylesheet" />
	<link href="${cadastroCss}" rel="stylesheet" />
  	<script src="${jQuery}"> </script>
  	<script src="${mask}"> </script>
  	<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
  	<script>
  	$(document).ready(function(){
  		$('#cepUsuario').mask('00.000-000');
  		$('#botContinuar').click(function() {
  			$('#cadastroInicio').submit();
  		});
  		var erros = $('#erros');
  		if($('#grupoErros').children('div').length > 0){
  			erros.animate({
  				bottom: 0
  			}, 600);
  		}
  		$('#fecharErro').click(function() {
  			erros.animate({
  				bottom: '-1000px'
  			}, 600);
  		});
  		
  		
  	});
  	</script>
 </head>
   
 <body>
   <header>
   	<nav>
   		<div id="cabecalhoParteEsquerda">
   			<c:url var="index" value="/" />
				<a href="${index }" style="border: none;"><img src="${logotipo}"
					alt="Mesus" /></a>
   		</div>
   		<c:url var="irpainel" value="/area/principal" />
   		<div id="cabecalhoParteDireita">
   			<div><a href="sobre">Sobre</a></div>
   			<div><a href="pr">Perguntas &amp; Respostas</a></div>
   			<div><a href="${irpainel}">Entrar</a></div>
   		</div>
   	</nav>
   </header>
   <section>
	   <div id="faixaCentral">
	   		<div id="passos">
	   		<div class="numeroPasso numeroPassoPosicaoAtual">1</div>
	   		<div class="traco">____________________</div>
	   		<div class="numeroPasso">2</div>
	   		<div class="traco">____________________</div>
	   		<div class="numeroPasso">3</div>
	   	</div>
		<h1>Entre com seu e-mail e CEP para começar!</h1>
		<h2>Seja parte da luta contra a fome.</h2>
		
	</div>
	
   </section>
   <form:form method="POST" modelAttribute="primeiroPasso" action="email_cep" id="cadastroInicio">
			<div id="erros">
			<div style="text-align: right"><img id="fecharErro" src="${fechar}" alt="fechar" /></div>
				<div id="grupoErros">
					<form:errors path="email" element="div" />
					<form:errors path="CEP" element="div" />
				</div>
			</div>
			<div>
				<div class="rotulo">e-mail</div>
				<div>
					<form:input path="email" placeholder="E.g.  lorem@ipsum.com" id="emailUsuario" type="email" />
					
				</div>
			</div>
			<div>
				<div class="rotulo">CEP</div>
				<div>
					<form:input path="CEP" type="email" placeholder="E.g.   12.345-532" id="cepUsuario" />
					<div id="botContinuar" class="botao">Continuar</div>
				</div>
				
			</div>
			
	</form:form>
    
   
 </body>
 </html>