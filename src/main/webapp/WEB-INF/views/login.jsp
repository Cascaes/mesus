<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Nada de prato vazio!</title>
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_login.css" var="loginCss" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/imagens/favicon" var="favicon" />
<spring:url value="/resources/imagens/loading_principal.gif" var="carregando" />
<jsp:include page="favicons.jsp" />


<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${loginCss}" rel="stylesheet" />
<script src="${jQuery}"> </script>
<script src="${mask}"> </script>
<script>
function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}
$(document).ready(function() {
	if(inIframe()) {
		window.parent.location.reload();
	}
	
	$('#blocoLogin').removeClass('erro').addClass('padrao');
	$('body').removeClass('padraoErro').addClass('padraoFundo');
	$('#esqueciConta').click(function() {
		$('#esquecii').show(function() {
			$('#esquecii').animate({
				bottom: '50px'
			}, {
				duration: 100
			});
		});
		
	});
	$('#esquecii i').click(function() {
		$('#esquecii').animate({
			bottom: '-500px'
		});
	});
	$('#esqueciIniciar').click(function() {
		$.ajax({
			url: 'procedimentolembrar',
			type: 'POST',
			beforeSend: function() {
				$('input').prop('disabled', true);
				$('.botao').fadeOut(function() {
					$('#carregando').fadeIn();
				});
			},
			data: {cpf: $('#cpf').val() },
			success: function(retorno) {
				$('input').prop('disabled', false);
				$('#carregando').fadeOut(function() {
					$('.botao').fadeIn(function() {
						$('#esquecii i').click();
					});
				});
			}
		});
	});
	$('#cpf').mask('000.000.000-00');
});
</script>
</head>
<c:choose>
	<c:when test="${param.error != null}">
		<body class="padraoErro">
	</c:when>
	<c:otherwise>
		<body class="padraoFundo">
	</c:otherwise>
</c:choose>
<header>
	<nav>
		<div id="cabecalhoParteEsquerda">
			<c:url var="index" value="/" />
			<a href="${index }" style="border: none;"><img src="${logotipo}"
				alt="Mesus" /></a>
		</div>

	</nav>
</header>
<section>

	<c:url var="loginUrl" value="j_spring_security_check" />
	<c:choose>
		<c:when test="${param.error != null}">
			<form action="${loginUrl}" class="erro" method="post" id="blocoLogin">
				<div>${param.error}
					<p>
						<spring:message code="login.erro.emailsenhainativo" />
					</p>
				</div>
		</c:when>
		<c:otherwise>
			<form action="${loginUrl}" class="padrao" method="post"
				id="blocoLogin">
		</c:otherwise>
	</c:choose>



	<c:if test="${param.logout != null}">
		<div>
			<p>
				<spring:message code="logout.sucesso" />
			</p>
		</div>
	</c:if>
	<div class="primeirosDados">
		<input type="text" id="email" name="j_username" placeholder="E-mail" />
	</div>
	<div class="primeirosDados">
		<input type="password" id="senha" name="j_password"
			placeholder="Senha" />
	</div>
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />

	<div>
		<input class="botao" type="submit" id="botaoEntrar" value="Entrar" />
	</div>
	<div>
		<c:url var="loginUrl" value="/login" />
		<a id="esqueciConta">Esqueci e-mail e/ou senha...</a>

		<c:url var="criarNova" value="/minhaconta" />
		<a id="criarConta" href="${criarNova }">Criar conta</a>
	</div>
	</form>
	<div id="esquecii">
	<div style="text-align: right;"><i  class="material-icons">close</i></div>
		<div>
			<input type="text" id="cpf"  placeholder="Digite o CPF cadastrado" />
		</div>
		<div class="botao" id="esqueciIniciar">Iniciar</div>
		<div id="carregando"><img src="${carregando }" alt="carregando..." /></div>
	</div>
</section>
</body>
</html>
