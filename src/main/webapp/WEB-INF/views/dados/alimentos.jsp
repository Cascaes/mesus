<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<div><i class="material-icons">close</i></div>
<ol>
<c:forEach items="${alimentos }" var="alimento">
	<li>${alimento.nome}</li>
</c:forEach>
</ol>