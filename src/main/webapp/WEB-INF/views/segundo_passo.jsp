<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Nada de prato vazio!</title>
	<spring:url value="/resources/css/style.css" var="coreCss" />
	<spring:url value="/resources/css/style_cadastro.css" var="cadastroCss" />
	<spring:url value="/resources/css/style_cadastro2.css" var="segundoPassoCss" />
	<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
	<spring:url value="/resources/imagens/fechar.png" var="fechar" />
	<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
	<spring:url value="/resources/js/mask.js" var="mask" />
	<spring:url value="/resources/ps" var="perfectscrollbar" />
	<spring:url value="/resources/imagens/favicon" var="favicon" />
	
	
	<jsp:include page="favicons.jsp" />
	
	<link rel="stylesheet" href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="${coreCss}" rel="stylesheet" />
	<link href="${cadastroCss}" rel="stylesheet" />
	<link href="${segundoPassoCss}" rel="stylesheet" />
  	<script src="${jQuery}"> </script>
  	<script src="${mask}"> </script>
  	<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
  	<script>
  	$(document).ready(function(){
  		$('#nascimento').mask('00/00/0000');
  		$('#cpf').mask('000.000.000-00');
  		var SPMaskBehavior = function (val) {
  		  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  		},
  		spOptions = {
  		  onKeyPress: function(val, e, field, options) {
  		      field.mask(SPMaskBehavior.apply({}, arguments), options);
  		    }
  		};

  		$('#telefone').mask(SPMaskBehavior, spOptions);
  		$('#botContinuar').click(function() {
  			$('#cadastroInicio').submit();
  		});
  		var erros = $('#erros');
  		if($('#grupoErros').children('div').length > 0){
  			erros.animate({
  				bottom: 0
  			}, 600);
  		}
  		$('#fecharErro').click(function() {
  			erros.animate({
  				bottom: '-1000px'
  			}, 600);
  		});
  		
  		
  	});
  	</script>
 </head>
   
 <body>
   <header>
   	<nav>
   		<div id="cabecalhoParteEsquerda">
   			<c:url var="index" value="/" />
				<a href="${index }" style="border: none;"><img src="${logotipo}"
					alt="Mesus" /></a>
   		</div>
   		<c:url var="irpainel" value="/area/principal" />
   		<div id="cabecalhoParteDireita">
   			<div><a href="sobre">Sobre</a></div>
   			<div><a href="pr">Perguntas &amp; Respostas</a></div>
   			<div><a href="${irpainel}">Entrar</a></div>
   		</div>
   	</nav>
   </header>
   <section>
	   <div id="faixaCentral">
	   		<div id="passos">
	   		<div class="numeroPasso">1</div>
	   		<div class="traco">____________________</div>
	   		<div class="numeroPasso numeroPassoPosicaoAtual">2</div>
	   		<div class="traco">____________________</div>
	   		<div class="numeroPasso">3</div>
	   	</div>
	</div>
   </section>
   <form:form method="POST" modelAttribute="segundoPasso" action="cadastro_geral" id="cadastroInicio">
		
		<div>
			<div class="rotulo">Nome completo*</div>
			<div class="primeirosDados">
				<form:input path="nome" />
			</div>
		</div>
		<div>
			<div class="rotulo">Sexo*</div>
			<div class="primeirosDados">
				<form:radiobutton path="genero" value="M"/>Masculino
				<form:radiobutton cssStyle="margin-left: 50px;" path="genero" value="F"/>Feminino
			</div>
		</div>
		<div>
			<div class="rotulo">Data de nascimento*</div>
			<div class="primeirosDados">
				<form:input path="nascimento" />
			</div>
		</div>
		<div>
			<div class="rotulo">Telefone*</div>
			<div class="primeirosDados">
				<form:input path="telefone" />
			</div>
		</div>
		<div>
			<div class="rotulo">CPF*</div>
			<div class="primeirosDados">
				<form:input path="cpf" />
			</div>
		</div>
		<div>
			<div class="rotulo">e-mail</div>
			<div class="primeirosDados">
				${primeiroPasso.email }
			</div>
		</div>
		<div>
			<div class="rotulo">CEP</div>
			<div class="primeirosDados">
				${primeiroPasso.CEP }
			</div>
			
		</div>
		<div>
			<div class="rotulo">Logradouro</div>
			<div class="primeirosDados">
				${logradouro } 
			</div>
		</div>
		<div>
			<div class="rotulo">Número</div>
			<div class="primeirosDados">
				<form:input path="numero" />
			</div>
		</div>
		<div>
			<div class="rotulo">Complemento</div>
			<div class="primeirosDados">
				<form:input path="complemento" />
			</div>
		</div>
		<div>
			<div class="rotulo">Bairro</div>
			<div class="primeirosDados">
				${bairro }
			</div>
		</div>
		<div>
			<div class="rotulo">Cidade/Estado</div>
			<div class="primeirosDados">
				<c:out value="${localidade}" />-<c:out value="${uf }" />
			</div>
		</div>
		<div>
			<div class="primeirosDados">
				<div id="botContinuar" class="botao">Continuar</div>
			</div>
		</div>
		<div id="erros">
			<div style="text-align: right"><img id="fecharErro" src="${fechar}" alt="fechar" /></div>
			<div id="grupoErros">
				<form:errors path="nome" element="div" />
				<form:errors path="nascimento" element="div" />
				<form:errors path="cpf" element="div" />
				<form:errors path="numero" element="div" />
				
			</div>
		</div>
	</form:form>
    
   
 </body>
 </html>
