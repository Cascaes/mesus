<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Nada de prato vazio!</title>
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_principal.css"
	var="principalCss" />
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/favicon" var="favicon" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/js/mesus.js" var="mesusJs" />
<spring:url value="/resources/vegas" var="vegas" />
<spring:url value="/resources/ps" var="perfectscrollbar" />


<jsp:include page="favicons.jsp" />

<link rel="stylesheet" href="${vegas}/vegas.min.css" />
<link rel="stylesheet"
	href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${principalCss}" rel="stylesheet" />
<script src="${jQuery}"> </script>
<script src="${vegas}/vegas.min.js"> </script>
<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
<script src="${mesusJs}"> </script>
</head>

<body>
	<header>
		<nav>
			<div id="cabecalhoParteEsquerda">
				<div><a href="sobre">Sobre</a></div>
				<div><a href="pr">Perguntas &amp; Respostas</a></div>
				<div><a class="refesp" href="#comoFunciona">Como funciona?</a></div>
			</div>
			<div id="cabecalhoParteMeio">
				<img src="${logotipo}" alt="Mesus" />
			</div>
			<div id="cabecalhoParteDireita">
				<c:set var="minhaconta" value="${true}" />
				<sec:authorize access="hasRole('PAPEL_USUARIO')">
					<c:set var="minhaconta" value="${false}" />
				</sec:authorize>
				<div>
					<c:choose>
						<c:when test="${minhaconta }">
							<a href="minhaconta">Minha conta</a>
						</c:when>
						<c:otherwise>
							<a href="logout">Sair</a>
						</c:otherwise>
					</c:choose>

				</div>
			</div>
		</nav>
	</header>
	<section>
		<div id="faixaCentral">
			<div id="mensagemConvite">
				<h1>Doar alimentos nunca foi tão fácil!</h1>
				<div>
				<c:url var="irpainel" value="/area/principal" />
					<div class="suaFrente">
						<a href="${irpainel}"><img src="${frente}/fornecedor.png" alt="Ícone de fornecedor" /></a>
						<div>Doar alimentos</div>
					</div>
					<div class="suaFrente">
						<a href="${irpainel}"><img src="${frente}/clienteprato.png" alt="Ícone de receptor" /></a>
						<div>Receber alimentos</div>
					</div>
				</div>

			</div>
			<h2>Bora acabar com a fome!</h2>
		</div>
	</section>


	<section>
		<div id="numerosSituacao">
			<div>
				<div>1.200 Ton.</div>
				<div>Alimentos desperdiçados somente no PR (2015)</div>
			</div>
			<div>
				<div>3,4 Milhões</div>
				<div>Pessoas passam fome no Brasil (2014)</div>
			</div>
		</div>
	</section>
	<section>
		<div id="videoInspirador">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/KAzhAXjUG28?rel=0"></iframe>
		</div>
	</section>

	<section id="blocoInfo">
		<div id="fraseOquefazemos">Nossa tecnologia permite conectar
			entidades produtoras de alimento e entidades receptoras de alimentos
			via mapa de rota, que a entidade doadora pode abater o imposto e ao
			mesmo tempo ajudar a dirimir o problema da fome, da
			discrep&acirc;ncia de distribui&ccedil;&atilde;o alimentar!</div>
		<div id="subtitulo1">Como funciona?</div>
		<div id="blocoFunciona">
			<div id="subtituloDoando">DOANDO ALIMENTOS</div>
			<div id="subtituloRecebendo">RECEBENDO ALIMENTOS</div>
		</div>
		<div id="comoFunciona">
			<div id="conteudoComoFunciona">
				<div class="comoDoando">
					<div>
						<div class="subtitulo2">1. Poste a sua doação</div>
						<div>
							<i class="material-icons">laptop</i> <i class="material-icons">desktop_windows</i>
							<i class="material-icons">phone_android</i>
						</div>
						<div class="mensagemComo">
							<p>Solicitar uma coleta do seu excedente de alimentos através
								da nossa plataforma.</p>
						</div>
					</div>
					<div>
						<div class="subtitulo2">2. Você aparecerá no mapa</div>
						<div>
							<i class="material-icons">location_on</i> <i
								class="material-icons">map</i>
						</div>
						<div class="mensagemComo">
							<p>O receptor vai resgatar e entregar a comida para
								a entidade próxima em necessidade.</p>
						</div>
					</div>
					<div>
						<div class="subtitulo2">3. Veja o seu impacto!</div>
						<div>
							<i class="material-icons">favorite_border</i> <i
								class="material-icons">favorite</i>
						</div>
						<div class="mensagemComo">
							<p>Observe o seu impacto na sociedade!</p>
						</div>
					</div>
				</div>
				<div class="comoRecebendo">
					<div>
						<div class="subtitulo2">1. Cadastre-se no Mesus</div>
						<div>
							<i class="material-icons">assignment_ind</i>
						</div>
						<div class="mensagemComo">
							<p>Registre-se e busque num mapa as doações disponíveis perto de você.</p>
						</div>
					</div>
					<div>
						<div class="subtitulo2">2. Até 24 horas</div>
						<div>
							<i class="material-icons">timelapse</i>
						</div>
						<div class="mensagemComo">
							<p>Após a reserva, você tem até 24 horas para buscar a doação.</p>
						</div>
					</div>
					<div>
						<div class="subtitulo2">3. Missão cumprida!</div>
						<div>
							<i class="material-icons">favorite</i>
						</div>
						<div class="mensagemComo">
							<p>E tenham os alimentos e... acabem com o problema da fome!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
	<div class="links">
		<div><a href="sobre">Sobre</a></div>
		<div><a href="pr">Perguntas &amp; Respostas</a></div>
		<div><a class="refesp" href="#comoFunciona">Como funciona?</a></div>
	</div>
	<div class="links">
		<div><a href="minhaconta">Minha conta</a></div>
		<div><a href="mailto:contato@mesus.eco.br">Fale conosco - contato@mesus.eco.br</a></div>
		<div>Android</div>
	</div>
	<div id="direitoreserv">Todos os direitos reservados &copy;</div>
	</footer>
</body>
</html>
