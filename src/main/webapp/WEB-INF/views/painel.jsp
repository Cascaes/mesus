<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Nada de prato vazio!</title>
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/imagens/loading_principal.gif" var="carregando" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/imagens/favicon" var="favicon" />

<jsp:include page="favicons.jsp" />


<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${painelCss}" rel="stylesheet" />
<script src="${jQuery}"> </script>
<script>
	var opcoes;
	$(document).ready(function() {
		opcoes = $('.botaoNormal, .botaoEspecial');
		opcoes.click(function() {
			var opcao = $(this);
			$('iframe').attr('src', opcao.data('link'));
			opcoes.removeClass('marcado').removeClass('marcadoTexto');
			$('#carregando').show();
			$('#logotipo').hide();
			if(opcao.attr('class') == 'botaoEspecial')
				opcao.addClass('marcado');
			else
				opcao.addClass('marcadoTexto');	
		});
	});
</script>
</head>

<body>
	<header>
		<nav>
			<div id="cabecalhoParteEsquerda">
				<c:url var="index" value="/" />
				<a href="${index }" style="border: none;" id="logotipo"><img src="${logotipo}"
					alt="Mesus" /></a>
					<div id="carregando"><img src="${carregando }" alt="carregando..." /></div> <span>Ol&aacute; ${usuario.nome }!</span>
					
			</div>
			<div id="cabecalhoParteDireita">
				<c:url var="sair" value="/logout" />
				<a href="${sair}">Sair</a>
			</div>
		</nav>
	</header>
	<section>
		<iframe src="doar"></iframe>
		<nav id="opcaoPainel">
			<div class="botaoEspecial marcado" data-link="doar">
				<img src="${frente}/fornecedor.png" alt="Doar"> DOAR
			</div>
			<div class="botaoEspecial" data-link="receberDoacao">
				<img src="${frente}/clienteprato.png" alt="Receber Alimento">
				RECEBER
			</div>
			<div class="botaoNormal" data-link="listaDoacoes">Minhas doações</div>
			<sec:authorize access="hasRole('PAPEL_RECEPTOR')">
				<div class="botaoNormal" data-link="listaReservas">Doações
					reservadas</div>
			</sec:authorize>
			<div class="botaoNormal" data-link="contato">Contato</div>
		</nav>
	</section>
</body>
</html>
