<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Nada de prato vazio!</title>
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/imagens/loading_principal.gif" var="carregando" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/imagens/fundo_principal/fome_brasil5.jpg" var="fundoBonito" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/imagens/favicon" var="favicon" />

<jsp:include page="favicons.jsp" />


<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<script src="${jQuery}"> </script>
<style>
body {
	background-image: url(${fundoBonito});
	background-size: cover;
	background-attachment: fixed;
}
pre {
	width: 600px;
	font-family: 'Ubuntu', sans-serif;
	font-size: 14px;
	font-weight: 600;
	text-align: justify;
	position: absolute;
	background: rgba(255, 255, 255, 0.8);
	padding: 20px;
	word-spacing: 5px;
	letter-spacing: 2px;
	color: #333;
	box-shadow: 2px 2px 5px 0px rgba(255, 255, 255, 0.75);
	border-radius: 10px; left : 50%;
	margin-bottom: 30px;
	margin-left: -300px;
	left: 50%;
	white-space: pre-wrap;
}
header {
	position: fixed;
}
</style>
</head>

<body>
	<header>
		<nav>
			<div id="cabecalhoParteEsquerda">
				<c:url var="index" value="/" />
				<a href="${index }" style="border: none;"><img src="${logotipo}"
					alt="Mesus" /></a>
			</div>
		</nav>
	</header>
	<section>
		<pre>P: Quem pode doar?
R: Qualquer pessoa (física ou jurídica). Se for pessoa física, a pessoa deve ter, pelo menos, 18 anos de idade completos e um CPF válido.

P: O que posso doar?
R: Qualquer tipo de alimento, mas com algumas ressalvas:
	a. Não pode ser um alimento já manipulado (isto é cozido, já preparado anteriormente para uma refeiçao).
	b. Não pode estar fora do prazo de validade, ou com vencimento na data da doação.
	c. Os alimentos, tais como frutas, leguminosas, tubérculos, etc., devem estar inteiros. 
	d. No caso de alimentos embalados, a embalagem deve estar fechada, não tendo sido aberta em qualquer momento anterior.

P: Tenho um limite mínimo de doação?
R: Não existe qualquer limite, mínimo ou máximo para doação. Você pode doar de uma batata até 50 sacas de arroz.

P: Posso cancelar uma doação que eu fiz?
R: Se a sua doação ainda não foi direcionada a alguma instituição você pode, em qualquer momento cancelar a doação. Claro que, o nosso desejo, é que seu alimento doado chegue àqueles que precisam, mas entendemos que problemas podem acontecer, e muitas vezes o alimento pode estragar antes do prazo, um pacote pode rasgar-se, entre outras coisas.

P: Tenho que levar a minha doação em algum lugar?
R: Não. A partir do instante em que você cadastra a sua doação, uma instituição receptora é notificada, e ela tem a responsabilidade de ir ao seu endereço buscar a doação. Por isso é sempre importante manter seu cadastro atualizado.

RECEPÇÃO DA DOAÇÃO:

P: Quem pode receber?
R: Qualquer instituição com um CNPJ válido e ativo, e que seja reconhecida como um entidade Pública.

P: Que tipos de alimentos posso receber?
R: Todos os tipos de alimentos, sendo estes alimentos in natura, ainda não manipulados. No caso de alimentos embalados, os mesmos devem estar dentro do prazo de validade e com a embalagem intacta.

P: Posso escolher os alimentos que quero receber?
R: Não. A medida em que doações, das mais diversas, forem surgindo, elas serão distribuidas para as instituições, sem a possibilidade de escolhas ou filtragens da doação.

P: Como é escolhida a instituição que receberá a doação?
R: Existem alguns critérios que o próprio sistema estabelecerá. Os principais deles são:
   a) Fila. O sistema criará uma "fila", para que todas as instituições possam receber doações.
   b) Proximidade do local a ser retirada a doação. Para não gerar tanto gasto e deslocamentos longos, o sistema verificará qual instituição, que está na fila, está mais próxima da doação.
   c) Rejeição da doação. Se uma instituição rejeita muitas doações que são destinadas a ela, ela naturalmente ficará mais para o final da fila, pois o sistema sempre dará prioridade a instituições que recebem todo e qualquer tipo de doação.


P: Quem é responsável pelo transporte?
R: A instituição Receptora é responsável pelo transporte.

P: Como sou avisado das doações disponíveis?
R: Se você tiver somente acesso ao site, o aviso de recebimento da doação chegará via e-mail. Caso tenha o aplicativo para smartphone instalado, o mesmo emitirá um aviso no celular.

P: Sou obrigado a buscar toda e qualquer doação destinada a mim?
R: Você pode rejeitar determinadas doações. Compreendemos que, muitas vezes pode estar complicado o deslocamento, falta de transporte, entre outras coisas. Mas o sistema sempre dará prioridade a instituições que sempre buscam as doações, colocando para o final da fila as instituições que rejeitam com frequência as doações destinadas a ela.</pre>
	</section>
</body>
</html>
