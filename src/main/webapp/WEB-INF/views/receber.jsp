<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@taglib
	uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/css/style_cadastro2.css"
	var="formatarForms" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/loading.gif" var="loading" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/js/mask.js" var="mask" />
<spring:url value="/resources/ps" var="perfectscrollbar" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${formatarForms}" rel="stylesheet" />
<link href="${painelCss}" rel="stylesheet" />
<style>
#blocoMaisDados, #blocoCep {
	display: none;
}
#carr1, #carr2 {
	display: none;
	vertical-align: middle;

}
.primeirosDados {
	margin-bottom: 20px;
}
#lei {
	padding: 10px 0 20px 0;
	font-size: 20px;
	color: #3FA376;
}
#erroCEP {
	color: red;
	font-size: 12px;
	display: none;
}
</style>
<script src="${jQuery}"> </script>
<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
<script src="${mask}"> </script>
<script>
$(document).ready(function() {
	var t, cnpj = $('#cnpj'), cep = $('#cep'), botSalvar = $('#botSalvar'), manipulado = $('#manipulado'), erroCEP = $('#erroCEP');
	
	var SPMaskBehavior = function (val) {
	  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
	  onKeyPress: function(val, e, field, options) {
	      field.mask(SPMaskBehavior.apply({}, arguments), options);
	    }
	};

	$('#telefone').mask(SPMaskBehavior, spOptions);
	
	cnpj.mask('00.000.000/0000-00');
	cep.mask('00.000-000');
	manipulado.change(function() {
		if($(this).is(':checked'))
			botSalvar.css({'background-color': '#373737'});
		else
			botSalvar.css({'background-color': '#DFDFDF'});
	});
	botSalvar.click(function() {
		if(manipulado.is(':checked')) {
			
			$('#cadastroReceptor').submit();
		}
	})
	cep.keyup(function() {
		erroCEP.fadeOut();
		if($(this).val().length === 10) {
			var dado = cep.val().replace(/[\.\/\-]/g, '');
			$.ajax({
				url: 'buscacep?numero=' + dado,
				beforeSend: function() {
					cep.prop('disabled', true);
					$('#carr2').fadeIn();
					$('#blocoMaisDados').fadeOut();
				},
				success: function(retorno) {
					$('#carr2').fadeOut();
					var jsonCEP = $.parseJSON(retorno);
					if(!jsonCEP.erro) {
						$('#logradouro').html(jsonCEP.logradouro);
						$('#cidadestado').html(jsonCEP.localidade + "-" + jsonCEP.uf);
						$('#bairro').html(jsonCEP.bairro);
						$('#blocoMaisDados').fadeIn();
					} else {
						erroCEP.fadeIn();
					}
					cep.prop('disabled', false);
				}
			});
		} else {
			$('#blocoMaisDados').fadeOut();
		}
	});
	cnpj.keyup(function() {
		if($(this).val().length === 18) { 
			clearTimeout(t);
			
			var dado = cnpj.val();
			$.ajax({
				url: 'buscacnpjusuario?numero=' + dado,
				beforeSend: function() {
					cnpj.prop('disabled', true);
					$('#blocoCep').fadeOut();
					$('#carr1').fadeIn();
				},
				success: function(retorno) {
					if(retorno === 'true') {
						$('#elegivel').html('<b style="color: red">Este CNPJ já foi cadastrado por outro usuário!</b>');
						cnpj.prop('disabled', false);
						$('#carr1').fadeOut();
					}
					else {
						$.ajax({
							url: 'cnpj?numero=' + dado.replace(/[\.\/\-]/g, ''),
							success: function(retorno) {
								$('#carr1').fadeOut();
								if(retorno === 'true') {
									$('#elegivel').html('<b style="color: green">Sim! Pode prosseguir com o cadastro abaixo...</b>');
									$('#blocoCep').fadeIn();
								}
								else {
									$('#elegivel').html('<b style="color: orange">Não. Empresa com requisitos necessários não atingidos.</b>');
									$('#blocoCep').fadeOut();
								}
								cnpj.prop('disabled', false);
								
							}
						});
					}
					
				}
			});
			
			
			
		} else {
			$('#elegivel').html('Checando...');
			clearTimeout(t);
			t = setTimeout(function() {
				$('#elegivel').html('Complete seu CNPJ, por favor!');
				$('#blocoMaisDados').fadeOut();
			}, 700);
		}
	});
	window.parent.$('#carregando').hide();
	window.parent.$('#logotipo').show();

});

</script>
</head>

<body>
	<c:url var="cD" value="/area/receptor" />
	<form:form method="POST" modelAttribute="crudReceptor" action="${cD}"
		id="cadastroReceptor">
		<div id="erros">
			<div style="text-align: right">
				<img id="fecharErro" src="${fechar}" alt="fechar" />
			</div>
			<div id="grupoErros">
				<form:errors path="cep" element="div" />
				<form:errors path="cnpj" element="div" />
			</div>
		</div>
		<div>
			<div id="lei">Cadastre a instituição que receberá as doações</div>
			<div class="rotulo">
				CNPJ*
			</div>
			<div class="primeirosDados">
				<div>
					<form:input path="cnpj" /> <img id="carr1" src="${loading}" />
				</div>
			</div>
		</div>
		<div>
			<div class="rotulo">
				Elegível? 
			</div>
			<div class="primeirosDados">
				<div id="elegivel">
					Digite seu CNPJ acima...
				</div>
			</div>
		</div>
		<div id="blocoCep">
			<div>
				<div class="rotulo">CEP do local de recepção das doações*</div>
				<div class="primeirosDados">
					<form:input path="cep" /> <img id="carr2" src="${loading}" /> <span id="erroCEP">CEP inexistente!</span>
				</div>
			</div>
		</div>
		<div id="blocoMaisDados">
			<div>
				<div class="rotulo">Logradouro</div>
				<div class="primeirosDados" id="logradouro">
					
				</div>
			</div>
			<div>
				<div class="rotulo">Número</div>
				<div class="primeirosDados">
					<form:input path="numero" />
				</div>
			</div>
			<div>
				<div class="rotulo">Complemento</div>
				<div class="primeirosDados">
					<form:input path="complemento" />
				</div>
			</div>
			<div>
				<div class="rotulo">Bairro</div>
				<div class="primeirosDados" id="bairro">
				</div>
			</div>
			<div>
				<div class="rotulo">Cidade/Estado</div>
				<div class="primeirosDados" id="cidadestado">
					
				</div>
			</div>
			<div>
				<div class="rotulo">Telefone*</div>
				<div class="primeirosDados">
					<form:input path="telefone" />
				</div>
			</div>
			<div>
				<div class="primeirosDados">
					<input type="checkbox" id="manipulado" /> Compromento-me que sou o responsável pelo CNPJ
				</div>
			</div>
			
			<div>
				<div class="primeirosDados">
					<div id="botSalvar">Salvar</div> * Obs.: após o sucesso do cadastro, será necessário refazer login.
				</div>
				
			</div>
		</div>
	</form:form>
</body>
</html>