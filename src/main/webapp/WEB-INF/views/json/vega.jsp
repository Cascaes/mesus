<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<spring:url value="/resources/vegas" var="vegas" />
<spring:url value="/resources/imagens/fundo_principal" var="colecao" />
<json:object>
	<json:array name="vega">
		<c:forEach begin="1" end="6" var="numIm">
			<json:object>
				<json:property name="src">${colecao}/fome_brasil${numIm}.jpg</json:property>
			</json:object>
		</c:forEach>
	</json:array>
	<json:property name="vega_overlay">${vegas}/overlays/02.png</json:property>
</json:object>
