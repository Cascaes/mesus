<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@taglib
	uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/css/style_cadastro2.css"
	var="formatarForms" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/js/mask.js" var="mask" />
<spring:url value="/resources/ps" var="perfectscrollbar" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${formatarForms}" rel="stylesheet" />
<link href="${painelCss}" rel="stylesheet" />
<style>
body, html {
	height: 500px;
}

textarea {
	height: 200px;
}

textarea, input {
	margin: 0 0 30px 30px;;
	font-family: 'Ubuntu', sans-serif;
	width: 380px !important;
}

#titulo {
	padding: 30px;
	font-family: 'Ubuntu', sans-serif;
	font-size: 16px;
	width: 400px;
	color: #E37C37;
	text-align: justify;
}

#botEnviar {
	width: 360px;
	border-radius: 5px;
	font-family: 'Ubuntu', sans-serif;
	color: #FFF;
	font-size: 20px;
	font-weight: 100;
	text-align: center;
	cursor: default;
	padding: 10px 20px 10px 20px;
	text-decoration: none;
	background: #373737;
	margin-left: 30px;
}

#feedb {
	background-color: #EEE;
	padding: 10px;
	box-shadow: 5px 5px 5px 0px rgba(50, 50, 50, 0.75);
	border-radius: 5px;
	position: absolute;
	display: none;
	font-size: 14px;
	left: 50%;
	margin-left: -100px;
	width: 200px;
	top: 40px;
}
#contateme {
	position: absolute;
	left: 50%;
	width: 500px;
	margin-left: -250px;
}
</style>
<script src="${jQuery}"> </script>
<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
<script src="${mask}"> </script>
<script>


$(document).ready(function() {
	$('#botEnviar').click(function() {
		window.parent.$('#carregando').show();
		window.parent.$('#logotipo').hide();
		$('input, textarea').prop('disabled', true);
		$.ajax({
			type: 'POST',
			url: 'faleconosco',
			data: { 
				assunto: $('#assunto').val(), 
				mensagem: $('#mensagem').val()
			},
			success: function(retorno) {
				window.parent.$('#carregando').hide();
				window.parent.$('#logotipo').show();
				$('input, textarea').prop('disabled', false);
				var ack;
				if(retorno == 'true') {
					$('input, textarea').val('');
					ack = '<span style="color: #070">Mensagem enviada com sucesso!</span>';
				} else {
					ack = '<span style="color: #700">Falha no envio! Tente novamente em instantes...</span>';
				}
				$('#feedb').html(ack);
				$('#feedb').fadeIn(function() {
					setTimeout(function() {
						$('#feedb').fadeOut();
					}, 4000);
				});
			}
		});
	});
	window.parent.$('#carregando').hide();
	window.parent.$('#logotipo').show();
});
</script>
</head>
<body>
	<div id="contateme">
		<div id="feedb"></div>
		<div id="titulo">Teve dificuldades em algum processo? Algum problema com alguma doação?
		Algum dado errado? Ou quer deixar algum elogio e sugestões? Fale conosco! 
		Iremos responder pelo e-mail cadastrado na sua conta!</div>
		<div><input maxlength="30" id="assunto" type="text" placeholder="Assunto:"></div>
		<div><textarea id="mensagem" placeholder="Digite aqui o que deseja falar..."></textarea></div>
		<div id="botEnviar">Enviar</div>
	</div>
</body>
</html>