<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@taglib
	uri="http://www.springframework.org/tags/form" prefix="form"%><%@ taglib
	prefix="sec" uri="http://www.springframework.org/security/tags"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	isELIgnored="false"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ page
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<spring:url value="/resources/imagens/frente" var="frente" />
<spring:url value="/resources/css/style.css" var="coreCss" />
<spring:url value="/resources/css/style_painel.css" var="painelCss" />
<spring:url value="/resources/css/style_cadastro2.css"
	var="formatarForms" />
<spring:url value="/resources/imagens/mesus.png" var="logotipo" />
<spring:url value="/resources/imagens/fornecedor2.png"
	var="coracao" />
<spring:url value="/resources/imagens/loading.gif" var="loading" />
<spring:url value="/resources/imagens/m" var="cluster" />
<spring:url value="/resources/imagens/fechar.png" var="fechar" />
<spring:url value="/resources/js/jquery.min.js" var="jQuery" />
<spring:url value="/resources/js/mask.js" var="mask" />
<spring:url value="/resources/ps" var="perfectscrollbar" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${perfectscrollbar}/css/perfect-scrollbar.min.css" />
<link href="${coreCss}" rel="stylesheet" />
<link href="${formatarForms}" rel="stylesheet" />
<link href="${painelCss}" rel="stylesheet" />
<style>
body, html {
	height: 500px;
}

#gmap {
	height: 600px;
	width: 600px;
	position: absolute;
	left: 50%;
	margin-left: -300px;
	top: 50px;
	box-shadow: 4px 4px 5px 0px rgba(50, 50, 50, 0.75);
}

#tit {
	font-family: 'Ubuntu', sans-serif;
	font-size: 16px;
	font-weight: 700;
	padding-top: 10px;
	color: #008080;
	font-size: 20px;
	text-align: center;
}


</style>
<script src="${jQuery}"> </script>
<script src="${perfectscrollbar}/js/perfect-scrollbar.jquery.min.js"> </script>
<script src="${mask}"> </script>
<script type="text/javascript" src="https://googlemaps.github.io/js-marker-clusterer/src/markerclusterer.js"> </script>
<script>
var googleMap;
var infowindow;
var markers = [];
var buscarDoacao;
function createMarker(latitude, longitude, html) {
	var coracao = {
		    url: '${coracao}'
	};
	var marker = new google.maps.Marker({
		position: {
			lat: latitude + (Math.random() -.5) / 7000,
			lng: longitude + (Math.random() -.5) / 7000
		},
		icon: coracao,
		map: googleMap,
		clickable : true
	});
	
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(googleMap, marker);
    });
    return marker;
}
function balao(n) {
	buscarDoacao.fadeIn();
	buscarDoacao.attr('data-buscar', n);
}

function initMap() {
	
	$.ajax({
		url: 'doadores',
		type: 'GET',
		success: function(json) {
			console.log(json);
			var dados = $.parseJSON(json);
			googleMap = new google.maps.Map(document.getElementById('gmap'), {
				center: {
					lat: dados.latitude,
					lng: dados.longitude
				},
				zoom: 8
			});
			var pineu = new google.maps.MarkerImage("http://www.googlemapsmarkers.com/v1/009900/");
			var marker = new google.maps.Marker({
			    position: { lat: dados.latitude, lng: dados.longitude },
			    map: googleMap,
			    icon: pineu,
			    title: 'Sua localidade'
		    });
			infowindow = new google.maps.InfoWindow();
			for (var i = 0; i < dados.listaDoacoes.length; i++) {
				try {
					var cada = dados.listaDoacoes[i];
					var peso = parseFloat(cada.gramas) / 1000;
					peso = peso.toFixed(2).replace('.', ',');
					var balao = '<div>';
					balao += '<div><b>Doador: </b>' + cada.nome + '</div>';
					balao += '<div><b>Grupo Alimentar: </b>' + cada.grupoAlimentar + '</div>';
					balao += '<div><b>Peso (Kg): </b>' + peso + '</div>';
					balao += '<div><b>Endereço: </b>' + cada.endereco + '</div>';
					balao += '</div>';
					markers.push(createMarker(cada.latitude, cada.longitude, balao));
				} catch(err) {
					continue;
				}
			}
			var markerCluster = new MarkerClusterer(googleMap, markers, {imagePath: '${cluster}'});
			window.parent.$('#carregando').hide();
			window.parent.$('#logotipo').show();
		}
	});
}

$(document).ready(function() {
	buscarDoacao = $('#buscarDoacao');
	$('#naobuscar').click(function() {
		buscarDoacao.fadeOut();
	});
	$('#simbuscar').click(function() {
		$.ajax({
			url: 'reservar',
			type: 'POST',
			data: { n: buscarDoacao.attr('data-buscar') },
			success: function() {
				location.reload();
			}
		});
	});
});
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByVmk_4JWtElhF0Pzmd-Ih_O0Tx2SeeuU&callback=initMap"> </script>
</head>

<body>
	<div id="tit">Clique em um dos marcadores do mapa para ver detalhes.</div>
	<div id="gmap"></div>
	
	<div id="buscarDoacao" class="popupDecisao">
		<div>Buscar a doação?</div>
		<div>
			<div id="simbuscar" class="popupSim">Sim</div>
			<div id="naobuscar" class="popupNao">Não</div>
		</div>
	</div>
</body>
</html>