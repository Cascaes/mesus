$(document).ready(function() {
    var largura = screen.width,
	conteudoComoFunciona = $('#conteudoComoFunciona'),
        subtituloDoando = $('#subtituloDoando'),
        subtituloRecebendo = $('#subtituloRecebendo');

    subtituloDoando.mouseover(function() {
    	conteudoComoFunciona.removeClass('flipped');
    });
    
    subtituloRecebendo.mouseover(function() {
    	conteudoComoFunciona.addClass('flipped');
    });
   
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        url: 'vega',
        dataType: 'json',
        success: function(imagens) {
            $('body').vegas({
                slides: imagens.vega,
                overlay: imagens.vega_overlay
            });
        }
    });
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 500);
        }
    });
   
});