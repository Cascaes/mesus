Termos de uso e Política de Privacidade do Mesus

O acesso ao conteúdo do Mesus, via site http://www.mesus.eco.br/ (“SITE”) ou aplicativo virtual para dispositivos móveis (App) dependerá da sua plena concordância com o Termo de Uso e Política de Privacidade (“TERMO DE USO”) aqui previstos, então a sua leitura e posterior aceitação são indispensáveis. Com uso do Mesus via site ou App, significa que você concorda com todos os termos desta Política, portanto não use-o caso não concorde com qualquer um dos termos.

O TERMO DE USO tem como objetivo estabelecer os direitos e obrigações dos doadores e entidades receptoras participantes, bem como parceiros fomentadores, e esclarecer qual o tratamento conferido aos dados e informações fornecidos por aqueles que se cadastrarem para acesso a qualquer área do SITE, conforme as regras e informações abaixo previstas:

1. Do Site e App Mesus
a) O Mesus é uma plataforma que proporciona a integração entre DOADOR e RECEPTOR de alimentos. Ambos são considerados PARTICIPANTES do site e App. 
b) O Mesus via site ou App é uma maneira inovadora e prática de viabilizar o combate ao desperdício com a colaboração de diversas empresas e pessoas. 
c) O site ou App não cobra nenhum tipo de comissão de nenhum PARTICIPANTE sobre as doações realizadas.
d) Embora não haja cobrança de nenhuma comissão, o site e App Mesus dependem também de recursos de terceiros para que possa manter sua plataforma.

2. UTILIZAÇÃO DA PLATAFORMA
a) Qualquer pessoa maior de 18 anos pode doar alimentos através do SITE, sendo expressamente proibida a participação de menores de 18 anos, mesmo que com a prévia autorização dos pais ou responsáveis. Em caso de irregularidade, o Mesus poderá cancelar o cadastro desta pessoa e suas eventuais doações ou ações realizadas no SITE ou APP.
b) Ademais, o PARTICIPANTE é o único responsável pela segurança e controle de sua conta (nome de usuário e senha), que é pessoal e intransferível. 
c) O PARTICIPANTE se compromete a manter seus dados sempre atualizados juntos ao Mesus, sendo responsável pela veracidade das informações ali dispostas.
d) Todos e quaisquer conteúdos enviados pelos usuários são de responsabilidade exclusiva do próprio usuário, não havendo qualquer responsabilidade solidária, subsidiária ou a qualquer título pelo site e App. Sendo assim, o Mesus não se responsabiliza por quaisquer danos de qualquer natureza, em decorrência de conteúdos impróprios, ofensivos ou que violem direitos previstos na legislação brasileira
e) Caso haja qualquer suspeita de uso indevido ou não autorizado de sua conta, o usuário deve notificar imediatamente o Mesus, pelo e-mail: contato@mesus.eco.br. 
f) As regras de utilização do Mesus podem ser alteradas conforme alteração da legislação vigente ou demandas do mercado.

3. REGRAS DE USO PARA DOADORES
a) Doador é o PARTICIPANTE interessado em doar alimentos através do SITE ou App Mesus.
b) Entende-se por doador a pessoa que deseja doar alimentos EXCLUSIVAMENTE inteiros e não manipulados 
c) O doador deverá observar as seguintes etapas e obrigações: 
I) Cadastrar-se no SITE informando seu nome completo, CPF/ CNPJ, e-mail, endereço completo, e telefone; 
II) Ler na íntegra e aceitar este TERMO DE USO e POLÍTICA DE PRIVACIDADE; 
III) Escolher o (s) alimento (s) que deseja doar; 
IV) Informar a quantidade a ser doada; 
V) Ter ciência de que só podem ser doados alimentos que estejam inteiros, que não foram ainda manipulados (cozidos, assados, modificados) e que estejam em plenas condições de uso;
VI) Ter ciência de que só podem ser doados alimentos que se enquadrem dentro das seguintes categorias: frutas, legumes, verduras, hortaliças, leguminosas secas, cereais secos, raízes e tubérculos;
VII) Ter ciência de que os alimentos doados devem ser indicados de acordo com as seguintes categorias: Arroz, raízes e tubérculos, legumes e verduras, frutas, leguminosas;


4. REGRAS DE USO PARA RECEPTORES
a) Receptor é o PARTICIPANTE interessado em receber os alimentos doados através do SITE ou App Mesus. 
b) O receptor deverá observar as seguintes etapas e obrigações: 
I) Cadastrar-se no SITE informando seu nome completo, CNPJ, e-mail, endereço completo, e telefone;
II) O Receptor obrigatoriamente deve estar cadastrado como entidade PÚBLICA, não sendo aceitos instituições de caráter PRIVADO e/ou empresas; 
III) Ler na íntegra e aceitar este TERMO DE USO; 
IV) Ler na íntegra e se comprometer em seguir recomendações da CVS 05/2013;
V) Escolher o (s) alimento (s) que deseja receber com frequência, ou selecionar para receber qualquer alimento;
VI) Ter ciência de que o transporte dos alimentos é de inteira responsabilidade do Receptor;
VII) Ter ciência de que o armazenamento, manuseio, cocção e utilização dos alimentos recebidos são de total responsabilidade do Receptor;
VIII) Ler na íntegra e fazer uso das recomendações da CVS 05/2013 sobre a correta higienização dos alimentos, bem como os cuidados necessários para evitar a contaminação cruzada estabelecidos pelo Centro de Vigilância Sanitária através da portaria acima mencionada.

5. CUIDADOS NECESSÁRIOS PARA HIGIENIZAÇÃO DOS ALIMENTOS
a) A limpeza consiste na operação de remoção de sujidades, substâncias minerais e ou orgânicas indesejáveis à qualidade do alimento, tais como terra, poeira, resíduos alimentares, gorduras, entre outras (CVS 5/2013);
b) A desinfecção é a operação por método físico e ou químico, de redução parcial do número de microrganismos patogênicos ou não, situados fora do organismo humano e não necessariamente matando os esporos (CVS 5/2013); 
c) A higienização é a operação que compreende duas etapas, a limpeza e a desinfecção (CVS 5/2013).
d) A higienização dos alimentos deve ser feita em local apropriado, com água potável e produtos desinfetantes para uso em alimentos, regularizados na ANVISA, e deve atender as instruções recomendadas pelo fabricante.
e) A higienização compreende a remoção mecânica de partes deterioradas e de sujidades sob água corrente potável, seguida de desinfecção por imersão em solução desinfetante. Quando esta for realizada com solução clorada, os hortifrutícolas devem permanecer imersos por quinze a trinta minutos, seguidos de enxágüe final com água potável.
f) As recomendações de diluições para a solução clorada desinfetante são:
I - dez mililitros ou uma colher de sopa rasa de hipoclorito de sódio na concentração de dois a dois vírgula cinco por cento, diluída em um litro de água potável;
II - vinte mililitros ou duas colheres de sopa rasas de hipoclorito de sódio na concentração de um por cento, diluídas em um litro de água potável.

6. POLÍTICA DE PRIVACIDADE 
a) O site e App Mesus foram desenvolvidos visando assegurar que a privacidade on-line seja respeitada e que as informações pessoais dos PARTICIPANTES sejam devidamente protegidas, sendo a presente política necessária para demonstrar o compromisso e a transparência do site Mesus quanto a este importante assunto.
b) Os dados fornecidos poderão ser utilizados em outros produtos que a empresa Mesus possua. 
c) O Mesus respeita o direito constitucional à privacidade, intimidade e autodeterminação informativa. Conheça a seguir as providências que tomamos para proteger as informações pessoais que os nossos usuários e visitantes poderão compartilhar conosco, assim como as formas de utilização destas informações e, com quem estas poderão ser compartilhadas:
I) NUNCA, em hipótese nenhuma, compartilharemos suas informações com terceiros. No caso do DOADOR, o seu nome e endereço serão compartilhados com a instituição RECEPTORA, para efeitos de busca da doação.
II) O acesso ao site, a partir do login é utilizando o protocolo HTTPS, o que torna a operação mais segura.

7. DÚVIDAS
Caso ainda tenha alguma dúvida, a Mesus está a disposição para receber e responder seus comentários e perguntas através do canal de ajuda disponibilizado no site http://www.mesus.eco.br. Você pode entrar em contato pelo e-mail: contato@mesus.eco.br. Teremos um enorme prazer em lhe ajudar a esclarecer quaisquer dúvidas.
