package pap.mesus.comunication;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EnvioEmail {
	private static final String SENDER = "contato@mesus.eco.br";
	private JavaMailSender mailSender;

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void reservaEfetuada(final String nome, final String emailReceptor, final Long identificador,
			final String grupoAlimentar, final int peso, final int validade, ServletContext servletContext) {

		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper htmlMailHelper = new MimeMessageHelper(message, true);
			htmlMailHelper.setTo(emailReceptor);
			htmlMailHelper.setFrom(SENDER);
			htmlMailHelper.setSubject(
					"Há uma doação reservada para você! Tem até 1 hora para confirmar o interesse no painel ou no app.");
			htmlMailHelper.setText("<table>" // "<img style='width: 50px;'
												// src='cid:logotipomesus' />" +
					+ "<tr><td colspan='2' style='color: #E37C37'>Ol&aacute; " + nome + "</td></tr>"
					+ "<tr><td style='font-weight: bold; width: 120px;'>Identificador: </td><td style='color: #E37C37'>"
					+ identificador + "</td></tr>"
					+ "<tr><td style='font-weight: bold; width: 120px;'>Grupo Alimentar: </td><td style='color: #E37C37'>"
					+ grupoAlimentar + "</td></tr>"
					+ "<tr><td style='font-weight: bold; width: 120px;'>Peso (g): </td><td style='color: #E37C37'>"
					+ peso + "</td></tr>", true);
			// FileSystemResource res = new FileSystemResource(
			// new
			// File(servletContext.getRealPath("/resources/imagens/mesus_menor.png")));
			// htmlMailHelper.addInline("logotipomesus", res);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		mailSender.send(message);
	}

	public void reservaCancelada(final String emailDoador, final String nome, final String emailReceptor,
			final Long identificador, ServletContext servletContext) {

		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper htmlMailHelper = new MimeMessageHelper(message, true);
			htmlMailHelper.setTo(emailDoador);
			htmlMailHelper.setFrom(SENDER);
			htmlMailHelper.setSubject("Uma sua doação foi cancelada!");
			htmlMailHelper.setText("<table>" // "<img style='width: 50px;'
												// src='cid:logotipomesus' />" +
					+ "<tr><td style='font-weight: bold; width: 120px;'>Receptor: </td><td style='color: #E37C37'>"
					+ nome + "</td></tr>"
					+ "<tr><td style='font-weight: bold; width: 120px;'>E-mail: </td><td style='color: #E37C37'><a href='mailto:"
					+ emailReceptor + "'>" + emailReceptor + "</a></td></tr>"
					+ "<tr><td style='font-weight: bold; width: 120px;'>Identificador: </td><td style='color: #E37C37'>"
					+ identificador + "</td></tr>" + "</table>" + "<div style='padding: 40px; font-size: 18px'>"
					+ "Equipe Mesus</div>", true);
			// FileSystemResource res = new FileSystemResource(
			// new
			// File(servletContext.getRealPath("/resources/imagens/mesus_menor.png")));
			// htmlMailHelper.addInline("logotipomesus", res);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		mailSender.send(message);
	}

	public void contato(final String from, final String subject, final String msg) {

		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper htmlMailHelper = new MimeMessageHelper(message, true);
			htmlMailHelper.setTo("caio@mesus.eco.br");
			htmlMailHelper.setFrom(SENDER);
			htmlMailHelper.setSubject(from + ": " + (subject.length() > 30 ? subject.substring(0, 30) : subject));
			htmlMailHelper.setText(msg, true);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
		mailSender.send(message);
	}

	public void refazerSenha(String partialUrl, String token, String email, ServletContext servletContext) {
		String url = partialUrl + "/trocarSenha?token=" + token;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper htmlMailHelper = new MimeMessageHelper(message, true);
			htmlMailHelper.setTo(email);
			htmlMailHelper.setFrom(SENDER);
			htmlMailHelper.setSubject("Recuperação de senha");
			htmlMailHelper.setText(// "<img style='width: 40px;'
									// src='cid:logotipomesus' />" +
					"<div style='padding: 10px; font-size: 18px'>" + "Processo de recuperação de senha"
							+ "</div><div style='padding: 10px; font-size: 14px'>"
							+ "Clique no link para iniciar o procedimento da recuperação de senha: <a href='" + url
							+ "'>" + url + "</a>"
							+ "</div><div style='padding: 10px; font-size: 14px'>O link expira em 24 horas.</div><div style='padding: 40px 0 0 10px; font-size: 14px'>"
							+ "Não foi você que pediu a recuperação de senha? Entre em contato conosco relatando respondendo esse e-mail!</div>"
							+ "<div style='padding: 40px; font-size: 18px'>" + "Equipe Mesus</div>",
					true);
			// FileSystemResource res = new FileSystemResource(
			// new
			// File(servletContext.getRealPath("/resources/imagens/mesus_menor.png")));
			// htmlMailHelper.addInline("logotipomesus", res);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		mailSender.send(message);
	}
}
