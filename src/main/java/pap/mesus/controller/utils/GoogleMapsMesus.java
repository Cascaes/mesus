package pap.mesus.controller.utils;

import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import atg.taglib.json.util.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pap.mesus.model.domain.Endereco;

public class GoogleMapsMesus {
	private static final String JSON_RESULTS = "results";

	private static final String JSON_GEOMETRY = "geometry";

	private static final String JSON_LOCATION = "location";

	private static final String JSON_LAT = "lat";

	private static final String JSON_LNG = "lng";

	public static void latLngEndereco(final Endereco enderecoDestino) {
		StringBuilder endereco = new StringBuilder();
		endereco.append(enderecoDestino.getLogradouro());
		endereco.append(", ");
		endereco.append(enderecoDestino.getNumero());
		endereco.append(", ");
		endereco.append(enderecoDestino.getBairro());
		endereco.append(", ");
		endereco.append(enderecoDestino.getCEP());
		try {
			String urlFormado = String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s",
					URLEncoder.encode(endereco.toString(), "UTF-8"), "AIzaSyByVmk_4JWtElhF0Pzmd-Ih_O0Tx2SeeuU");
			OkHttpClient client = new OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
					.readTimeout(1, TimeUnit.MINUTES).writeTimeout(1, TimeUnit.MINUTES).build();

			Request request = new Request.Builder().url(urlFormado).get().build();

			try {
				Response response = client.newCall(request).execute();
				if (response.isSuccessful()) {
					String result = response.body().string();
					JSONObject jObject = new JSONObject(result);
					JSONObject geral = jObject.optJSONArray(JSON_RESULTS).optJSONObject(0);
					JSONObject latlon = geral.optJSONObject(JSON_GEOMETRY).optJSONObject(JSON_LOCATION);
					enderecoDestino.setLatitude(Double.parseDouble(latlon.optString(JSON_LAT)));
					enderecoDestino.setLongitude(Double.parseDouble(latlon.optString(JSON_LNG)));
				}
			} catch (Exception e) {
			}
		} catch (Exception e) {
			return;
		}

	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	public enum Unidade {
		KM, M;
	}

	public static double distancia(double lat1, double lon1, double lat2, double lon2, Unidade unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == Unidade.KM) {
			dist = dist * 1.609344;
		} else if (unit == Unidade.M) {
			dist = dist * 0.8684;
		}
		return (dist);
	}
}
