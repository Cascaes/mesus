package pap.mesus.controller;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pap.mesus.controller.service.IAlimentoService;
import pap.mesus.controller.service.IDoacaoService;
import pap.mesus.controller.service.IService;
import pap.mesus.controller.service.IStatusService;
import pap.mesus.controller.service.IUsuarioService;
import pap.mesus.controller.validator.CRUDDoacaoUsuario;
import pap.mesus.model.domain.Alimento;
import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.GrupoAlimentar;
import pap.mesus.model.domain.Usuario;
import pap.mesus.model.domain.abstracts.CipherId;

@Controller
public class PrincipalController {

	public static final String INDEX = "index";

	public static final String RAIZ = "/";
	public static final String PADRAO = RAIZ + INDEX + ".html";
	private static final String VEGA = "/vega";
	private static final String JSON_VEGA = "json/vega";

	private static final String AREA_PRINCIPAL = "/area/principal";
	private static final String PAINEL = "painel";
	public static final String DADOS_USUARIO = "usuario";
	private static final String IFRAME_DOAR = "/area/doar";
	private static final String IFRAME_DOAR_CRUD = "/area/crud_doacao";
	private static final String DOAR = "doar";
	private static final String CRUD_DOACAO = "crudDoacao";
	private static final String GRUPO_ALIMENTAR = "grupoAlimentar";
	private static final String DADOS_ENQUADRADO = "/area/enquadrado";
	private static final String ALIMENTOS = "dados/alimentos";
	private static final String ALIMENTO = "alimentos";
	private static final String LISTA_DOACOES = "listaDoacoes";
	private static final String LISTA_DOACAO = "listaDoacao";
	private static final String IFRAME_LISTA_DOACAO = "/area/listaDoacoes";
	private static final String DOACAO = "doacao";
	private static final String IFRAME_DOAR_APAGAR = "/area/cancelar";
	private static final String SOBRE = "sobre";
	private static final String PERGUNTAS_RESPOSTAS = "pr";

	@Autowired
	CRUDDoacaoUsuario crudDoacaoUsuario;

	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IService<GrupoAlimentar> grupoAlimentarService;

	@Autowired
	IAlimentoService alimentoService;

	@Autowired
	IStatusService statusService;

	@Autowired
	IDoacaoService doacaoService;

	@RequestMapping(VEGA)
	public ModelAndView vega() {
		return new ModelAndView(JSON_VEGA);
	}

	@RequestMapping(value = { RAIZ, PADRAO })
	public ModelAndView index(ModelMap modelMap) {
		return new ModelAndView(INDEX);
	}

	@RequestMapping(value = { SOBRE })
	public ModelAndView sobre(ModelMap modelMap) {
		return new ModelAndView(SOBRE);
	}

	@RequestMapping(value = { PERGUNTAS_RESPOSTAS })
	public ModelAndView pr(ModelMap modelMap) {
		return new ModelAndView(PERGUNTAS_RESPOSTAS);
	}

	@RequestMapping(value = { AREA_PRINCIPAL })
	public ModelAndView painelPrincipal(ModelMap modelMap, HttpSession httpSession) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			Usuario u = usuarioService.buscarPorEmail(userDetail.getUsername());
			modelMap.addAttribute(DADOS_USUARIO, u);
			httpSession.setAttribute(DADOS_USUARIO, u);
		}
		return new ModelAndView(PAINEL);
	}

	@InitBinder
	public void dataBinding(WebDataBinder binder) {
		DecimalFormat decimalFormat = new DecimalFormat();
		decimalFormat.setGroupingUsed(false);
		binder.registerCustomEditor(Integer.class, new CustomNumberEditor(Integer.class, decimalFormat, true));
	}

	@RequestMapping(value = { IFRAME_DOAR_APAGAR })
	public ModelAndView iframeApagar(ModelMap modelMap, @RequestParam(required = false) String idEncriptado,
			HttpSession httpSession) {
		Integer id = Integer.parseInt(CipherId.descriptar(idEncriptado));
		doacaoService.apagarPorId(id);
		return iframeListarDoacao(modelMap, httpSession);

	}

	@RequestMapping(value = { IFRAME_DOAR })
	public ModelAndView iframeDoar(ModelMap modelMap, HttpSession httpSession,
			@RequestParam(required = false, defaultValue = "false") boolean edit,
			@RequestParam(required = false) String idEncriptado) {

		if (httpSession.getAttribute(GRUPO_ALIMENTAR) == null)
			httpSession.setAttribute(GRUPO_ALIMENTAR, grupoAlimentarService.listar());

		if (edit) {
			Integer id = Integer.parseInt(CipherId.descriptar(idEncriptado));
			Doacao d = doacaoService.buscarPorId(id);
			crudDoacaoUsuario.setGramas(d.getGramas());
			crudDoacaoUsuario.setGrupoAlimentarId(d.getGrupoAlimentar().getId());
			httpSession.setAttribute(DOACAO, d);
		} else {
			crudDoacaoUsuario = new CRUDDoacaoUsuario();
		}
		crudDoacaoUsuario.setEdit(edit);
		modelMap.addAttribute(CRUD_DOACAO, crudDoacaoUsuario);
		modelMap.addAttribute(GRUPO_ALIMENTAR, httpSession.getAttribute(GRUPO_ALIMENTAR));
		return new ModelAndView(DOAR);
	}

	@RequestMapping(value = { IFRAME_LISTA_DOACAO })
	public ModelAndView iframeListarDoacao(ModelMap modelMap, HttpSession httpSession) {

		modelMap.addAttribute(LISTA_DOACOES, doacaoService.listar((Usuario) httpSession.getAttribute(DADOS_USUARIO)));
		return new ModelAndView(LISTA_DOACAO);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { IFRAME_DOAR_CRUD }, method = RequestMethod.POST)
	public ModelAndView crudDoacao(ModelMap modelMap,
			@Valid @ModelAttribute(CRUD_DOACAO) CRUDDoacaoUsuario crudDoacaoUsuario, BindingResult bindingResult,
			HttpSession httpSession, @RequestParam(required = false, defaultValue = "false") boolean edit) {
		if (bindingResult.hasErrors()) {
			modelMap.addAttribute(GRUPO_ALIMENTAR, httpSession.getAttribute(GRUPO_ALIMENTAR));
			return new ModelAndView(DOAR);
		}
		Doacao doacao = httpSession.getAttribute(DOACAO) == null ? new Doacao()
				: (Doacao) httpSession.getAttribute(DOACAO);
		doacao.setGramas(crudDoacaoUsuario.getGramas());
		for (GrupoAlimentar grupoAlimentar : ((List<GrupoAlimentar>) httpSession.getAttribute(GRUPO_ALIMENTAR))) {
			if (grupoAlimentar.getId().equals(crudDoacaoUsuario.getGrupoAlimentarId())) {
				doacao.setGrupoAlimentar(grupoAlimentar);
				break;
			}
		}
		if (edit) {
			doacaoService.atualizar(doacao);
			httpSession.removeAttribute(DOACAO);

		} else {
			doacao.setStatus(statusService.statusInicial());
			doacao.setUsuario((Usuario) httpSession.getAttribute(DADOS_USUARIO));
			doacao.setData(new Date());
			doacaoService.salvar(doacao);
		}
		return iframeListarDoacao(modelMap, httpSession);
	}

	@RequestMapping(value = { DADOS_ENQUADRADO }, method = RequestMethod.GET)
	public ModelAndView infoAlimentos(@RequestParam Integer id) {
		List<Alimento> dados = alimentoService.listar(grupoAlimentarService.buscarPorId(id));
		ModelAndView mav = new ModelAndView(ALIMENTOS);
		mav.addObject(ALIMENTO, dados);
		return mav;
	}

}
