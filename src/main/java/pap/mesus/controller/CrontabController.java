package pap.mesus.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import pap.mesus.comunication.EnvioEmail;
import pap.mesus.controller.service.IDadosDoacoesService;
import pap.mesus.controller.service.IDoacaoService;
import pap.mesus.controller.service.IPessoaJuridicaService;
import pap.mesus.controller.service.IReservaService;
import pap.mesus.controller.service.IService;
import pap.mesus.controller.service.StatusService.Statuses;
import pap.mesus.controller.utils.GoogleMapsMesus;
import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.PessoaJuridica;
import pap.mesus.model.domain.Reserva;
import pap.mesus.model.domain.Usuario;
import pap.mesus.model.domain.view.DadosDoacoes;

@Controller
public class CrontabController {

	private static final int INICIO_HORARIO_COMERCIAL = 8;

	private static final int FIM_HORARIO_COMERCIAL = 18;

	@Autowired
	EnvioEmail envioEmail;

	@Autowired
	IDoacaoService doacaoService;

	@Autowired
	ServletContext servletContext;

	@Autowired
	IReservaService reservaService;

	@Autowired
	IService<Usuario> usuarioService;

	@Autowired
	IPessoaJuridicaService pessoaJuridicaService;

	@Autowired
	IDadosDoacoesService dadosDoacoesService;

	@RequestMapping("crontab")
	@ResponseStatus(HttpStatus.OK)
	public void script() { // throws IOException {
		Calendar rotina = Calendar.getInstance();
		int intervalo = rotina.get(Calendar.HOUR_OF_DAY);
		if (intervalo >= INICIO_HORARIO_COMERCIAL && intervalo <= FIM_HORARIO_COMERCIAL) {
			List<DadosDoacoes> dadosDoacoes = dadosDoacoesService.listaRotina();
			if (!dadosDoacoes.isEmpty()) {
				List<PessoaJuridica> pjl = pessoaJuridicaService.listar();
				if (!pjl.isEmpty()) {
					for (DadosDoacoes dd : dadosDoacoes) {
						Doacao dr = doacaoService.buscarPorId(dd.getIdDoacao());
						Reserva r = null;

						if (dd.getStatus().equals(Statuses.PROCESSO.name())) {
							r = reservaService.qual(dr);
							r.setRejeitada(true);
							reservaService.atualizar(r);

						}

						PessoaJuridica pjperto = null;
						Double perto = Double.MAX_VALUE;
						for (PessoaJuridica pj : pjl) {
							if ((r != null && r.getPessoaJuridica().getId() == pj.getId())
									|| dr.getUsuario().getId() == pj.getUsuario().getId())
								continue;

							double di = GoogleMapsMesus.distancia(dd.getLatitude(), dd.getLongitude(),
									pj.getEndereco().getLatitude(), pj.getEndereco().getLongitude(),
									GoogleMapsMesus.Unidade.M);
							if (di < perto) {
								perto = di;
								pjperto = pj;
							}

						}

						if (pjperto != null && dr.getUsuario().getId() != pjperto.getUsuario().getId()) {
							Reserva novar = new Reserva();
							novar.setDoacao(dr);
							novar.setPessoaJuridica(pjperto);
							novar.setData(new Date());
							reservaService.salvar(novar);
							envioEmail.reservaEfetuada(pjperto.getUsuario().getNome(), pjperto.getUsuario().getEmail(),
									dr.getIdentificador(), dd.getGrupoAlimentar(), dd.getGramas(), dr.getPrazoDias(),
									servletContext);
						}
					}
				}
			}
		}
	}
}
/*
 * 
 * UPDATE `mesus`.`DOACAO` SET `STATUS_ID` = '1';
 * 
 * DELETE FROM `mesus`.`RESERVA`;
 * 
 */
