package pap.mesus.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import pap.mesus.comunication.EnvioEmail;
import pap.mesus.controller.service.IPessoaFisicaService;
import pap.mesus.controller.service.IRecuperacaoSenhaService;
import pap.mesus.controller.service.IUsuarioService;
import pap.mesus.model.domain.PessoaFisica;
import pap.mesus.model.domain.RecuperacaoSenha;
import pap.mesus.model.domain.Usuario;

// http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/

@Controller
public class SegurancaController {

	private static final String TOKEN_RECUPERACAO = "rec_sen";

	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	IPessoaFisicaService pessoaFisicaService;

	@Autowired
	EnvioEmail envioEmail;

	@Autowired
	IRecuperacaoSenhaService recuperacaoSenhaService;

	@RequestMapping(value = "/403")
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		// Authentication auth =
		// SecurityContextHolder.getContext().getAuthentication();
		// if (!(auth instanceof AnonymousAuthenticationToken)) {
		/// UserDetails userDetail = (UserDetails) auth.getPrincipal();
		// model.addObject("username", userDetail.getUsername());
		// }

		model.setViewName("403");
		return model;

	}

	@RequestMapping(value = "/login")
	public ModelAndView login() {

		ModelAndView model = new ModelAndView();

		model.setViewName("login");
		return model;

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	@RequestMapping(value = "/trocarSenha", method = RequestMethod.GET)
	public ModelAndView paginaTrocaSenha(@RequestParam String token, HttpSession httpSession) {

		RecuperacaoSenha passToken = recuperacaoSenhaService.buscarPorToken(token);
		if (passToken == null) {
			return new ModelAndView(PrincipalController.INDEX);
		}

		Calendar cal = Calendar.getInstance();
		if ((passToken.getDataExpiracao().getTime() - cal.getTime().getTime()) <= 0) {
			recuperacaoSenhaService.apagar(passToken); // usar trigger em lugar
			return new ModelAndView(PrincipalController.INDEX);
		}
		httpSession.setAttribute(TOKEN_RECUPERACAO, passToken);
		return new ModelAndView("recuperar_senha");
	}

	@RequestMapping(value = "/nova_senha", method = RequestMethod.POST)
	public ModelAndView registrarTrocaSenha(@RequestParam String senha, HttpSession httpSession) {
		RecuperacaoSenha passToken = (RecuperacaoSenha) httpSession.getAttribute(TOKEN_RECUPERACAO);
		if (passToken == null) {

			return new ModelAndView(PrincipalController.INDEX);
		}
		BCryptPasswordEncoder criptografaSenha = new BCryptPasswordEncoder();
		Usuario u = passToken.getUsuario();
		u.setSenha(criptografaSenha.encode(senha));
		usuarioService.atualizar(u);
		recuperacaoSenhaService.apagar(passToken);
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/procedimentolembrar", method = RequestMethod.POST)
	@ResponseBody
	public void resetPassword(HttpServletRequest request, @RequestParam(required = true) String cpf) {
		System.out.println("Alif");
		PessoaFisica pf = pessoaFisicaService.buscarPorCPF(cpf);
		System.out.println("Ba");
		if (pf == null) {
			System.out.println("Tha");
			return;
		}
		System.out.println("Ta");
		String token = UUID.randomUUID().toString();
		RecuperacaoSenha rs = new RecuperacaoSenha();
		System.out.println("Jim");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, 24);
		System.out.println("Ha");
		rs.setDataExpiracao(cal.getTime());
		rs.setToken(token);
		rs.setUsuario(pf.getUsuario());
		recuperacaoSenhaService.salvar(rs);
		System.out.println("Hat");
		String url = "https://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
		envioEmail.refazerSenha(url, token, pf.getUsuario().getEmail(), servletContext);
		System.out.println("Tat");
	}
}
