package pap.mesus.controller;

import java.util.Date;
import java.util.HashSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import atg.taglib.json.util.JSONObject;
import pap.mesus.controller.service.IPapelService;
import pap.mesus.controller.service.IService;
import pap.mesus.controller.utils.GoogleMapsMesus;
import pap.mesus.controller.validator.PrimeiroPassoUsuario;
import pap.mesus.controller.validator.SegundoPassoUsuario;
import pap.mesus.controller.validator.TerceiroPassoUsuario;
import pap.mesus.model.domain.Endereco;
import pap.mesus.model.domain.Papel;
import pap.mesus.model.domain.PessoaFisica;
import pap.mesus.model.domain.Usuario;

@Controller
public class CadastroController {

	@Autowired
	ServletContext context;

	@Autowired
	SegundoPassoUsuario segundoPassoUsuario;

	@Autowired
	PrimeiroPassoUsuario primeiroPassoUsuario;

	@Autowired
	TerceiroPassoUsuario terceiroPassoUsuario;

	@Autowired
	private IPapelService papelService;

	@Autowired
	private IService<PessoaFisica> pessoaFisicaService;

	private static final String CADASTRO = "/cadastro_senha";

	private static final String TERCEIRO_PASSO_JSP = "terceiro_passo";

	private static final String TERCEIRO_PASSO = "terceiroPasso";

	@SuppressWarnings("serial")
	@RequestMapping(value = { CADASTRO }, method = RequestMethod.POST)
	public ModelAndView cadastroUsuario(
			@Valid @ModelAttribute(TERCEIRO_PASSO) TerceiroPassoUsuario terceiroPassoUsuario,
			BindingResult bindingResult, ModelMap modelMap, HttpSession httpSession) {
		if (bindingResult.hasErrors()) {
			modelMap.addAttribute(PassoAPassoController.PRIMEIRO_PASSO,
					httpSession.getAttribute(PassoAPassoController.PRIMEIRO_PASSO));
			modelMap.addAttribute(PassoAPassoController.SEGUNDO_PASSO,
					httpSession.getAttribute(PassoAPassoController.SEGUNDO_PASSO));
			modelMap.addAttribute(TERCEIRO_PASSO, terceiroPassoUsuario);
			return new ModelAndView(TERCEIRO_PASSO_JSP);
		}

		primeiroPassoUsuario = (PrimeiroPassoUsuario) httpSession.getAttribute(PassoAPassoController.PRIMEIRO_PASSO);
		segundoPassoUsuario = (SegundoPassoUsuario) httpSession.getAttribute(PassoAPassoController.SEGUNDO_PASSO);

		Usuario u = new Usuario();
		u.setDataCadastro(new Date());
		u.setEmail(primeiroPassoUsuario.getEmail());
		u.setAtivo(true);
		u.setNome(segundoPassoUsuario.getNome());

		BCryptPasswordEncoder criptografaSenha = new BCryptPasswordEncoder();

		u.setSenha(criptografaSenha.encode(terceiroPassoUsuario.getSenha()));
		Endereco e = new Endereco();
		JSONObject cep = (JSONObject) httpSession.getAttribute(PassoAPassoController.CEP);
		e.setBairro(cep.optString(PassoAPassoController.JSON_CEP_BAIRRO));
		e.setCidade(cep.optString(PassoAPassoController.JSON_CEP_CIDADE));
		e.setEstado(cep.optString(PassoAPassoController.JSON_CEP_ESTADO));
		e.setLogradouro(cep.optString(PassoAPassoController.JSON_CEP_RUA));
		e.setNumero(segundoPassoUsuario.getNumero());
		e.setCEP(primeiroPassoUsuario.getCEP());
		e.setComplemento(segundoPassoUsuario.getComplemento());
		GoogleMapsMesus.latLngEndereco(e);
		u.setUsuarioPapeis(new HashSet<Papel>() {
			{
				add(papelService.papelUsuario());
			}
		});

		PessoaFisica p = new PessoaFisica();
		p.setCpf(segundoPassoUsuario.getCpf());
		p.setUsuario(u);
		p.setTelefone(segundoPassoUsuario.getTelefone());
		p.setEndereco(e);
		p.setGenero(segundoPassoUsuario.getGenero());
		p.setDataNascimento(segundoPassoUsuario.getNascimento());
		pessoaFisicaService.salvar(p);

		return new ModelAndView(PrincipalController.INDEX);

	}

}
