package pap.mesus.controller.validator;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CRUDDoacaoUsuario implements Serializable {

	private static final long serialVersionUID = -6532016032584208776L;

	@NotNull(message = "Peso não pode ser vazio!")
	@NumberFormat(style = Style.NUMBER)
	@Min(value = 1, message = "Peso não pode ser vazio e deve ser maior ou igual a 1.")
	private Integer gramas;

	@NotNull
	private Integer grupoAlimentarId;

	private Integer idDoacao;

	private boolean edit;

	public Integer getGramas() {
		return gramas;
	}

	public void setGramas(Integer gramas) {
		this.gramas = gramas;
	}

	public Integer getGrupoAlimentarId() {
		return grupoAlimentarId;
	}

	public void setGrupoAlimentarId(Integer grupoAlimentarId) {
		this.grupoAlimentarId = grupoAlimentarId;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public Integer getIdDoacao() {
		return idDoacao;
	}

	public void setIdDoacao(Integer idDoacao) {
		this.idDoacao = idDoacao;
	}

}
