package pap.mesus.controller.validator;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SegundoPassoUsuario implements Serializable {
	private static final long serialVersionUID = -4400884373708940178L;

	public static class ErrorCPFExists {
		public static final String CAMPO_CPF = "cpf";
		public static final String MENSAGEM_CPF = "CPF já cadastrado!";

	}

	@NotEmpty(message = "Nome não pode ser vazio!")
	private String nome;

	private String numero;

	@NotNull(message = "Data não pode ser vazia!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date nascimento;

	private String complemento;

	@NotEmpty(message = "Telefone não pode ser vazio!")
	private String telefone;

	@NotEmpty(message = "Selecione o sexo!")
	private String genero;

	@CPF
	private String cpf;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
