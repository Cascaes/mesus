package pap.mesus.controller.validator;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TerceiroPassoUsuario implements Serializable {

	private static final long serialVersionUID = -169466824904602413L;

	// @NotEmpty
	// @Min(value = 6, message = "A senha tem que ter no mínimo 6 caracteres!")
	// @Pattern.List({ @Pattern(regexp = "[A-Z]", message = "Necessário ter pelo
	// menos uma letra maiúscula"),
	// @Pattern(regexp = "[a-z]", message = "Necessário ter pelo menos uma letra
	// minúscula"),
	// @Pattern(regexp = "[0-9]", message = "Necessário ter pelo menos um
	// número") })
	// @Min(value = 6, message = "A senha tem que ter no mínimo 6 caracteres!")
	private String senha;

	private String repetirsenha;

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getRepetirsenha() {
		return repetirsenha;
	}

	public void setRepetirsenha(String repetirsenha) {
		this.repetirsenha = repetirsenha;
	}

}
