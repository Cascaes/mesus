package pap.mesus.controller.validator;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import pap.mesus.controller.validator.CEP.CEP;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrimeiroPassoUsuario implements Serializable {

	private static final long serialVersionUID = -1457325409872603909L;

	public static class ErrorEmailExists {
		public static final String CAMPO_EMAIL = "email";
		public static final String CAMPO_CEP = "CEP";
		public static final String MENSAGEM_EMAIL = "E-mail já cadastrado no Mesus!";
		public static final String MENSAGEM_CEP = "CEP não existe!";

	}

	@Email(message = "E-mail inválido!")
	@NotEmpty(message = "E-mail não pode estar vazio!")
	private String email;

	@CEP(message = "CEP Inválido ou vazio! O formato deve ser: XX.XXX-XXX")
	private String CEP;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

}
