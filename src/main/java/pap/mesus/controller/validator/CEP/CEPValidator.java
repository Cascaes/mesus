package pap.mesus.controller.validator.CEP;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import atg.taglib.json.util.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CEPValidator implements ConstraintValidator<CEP, String> {
	protected static final String PESQUISA_CEP = "http://viacep.com.br/ws/%s/json/";
	protected static final String JSON_CEP = "cep";
	public static final String JSON_ERRO = "erro";

	@Override
	public void initialize(CEP arg0) {

	}

	@Override
	public boolean isValid(String cepInserido, ConstraintValidatorContext context) {

		return cepInserido != null
				&& (cepInserido.matches("^\\d{5}[-]\\d{3}$") || cepInserido.matches("^[0-9]{2}.[0-9]{3}-[0-9]{3}$"));

	}

	public static JSONObject pesquisa(String cep) {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url(String.format(PESQUISA_CEP, cep.replaceAll("\\.", ""))).get()
				.build();
		Response response = null;
		try {
			response = client.newCall(request).execute();
			if (response.isSuccessful()) {
				String result = response.body().string();
				JSONObject jsonObject = new JSONObject(result);
				if (jsonObject.has(JSON_CEP)) {
					jsonObject.put(JSON_ERRO, false);
				}
				return jsonObject;

			} else {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put(JSON_ERRO, true);
				return jsonObject;
			}
		} catch (Exception e) {
			return null;
		} finally {
			if (response != null)
				response.body().close();
		}

	}
}
