package pap.mesus.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONException;
import atg.taglib.json.util.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pap.mesus.controller.service.IDoacaoService;
import pap.mesus.controller.service.IPapelService;
import pap.mesus.controller.service.IPessoaFisicaService;
import pap.mesus.controller.service.IPessoaJuridicaService;
import pap.mesus.controller.service.IReservaService;
import pap.mesus.controller.service.IStatusService;
import pap.mesus.controller.service.IUsuarioService;
import pap.mesus.controller.utils.GoogleMapsMesus;
import pap.mesus.controller.validator.PrimeiroPassoPessoaJuridica;
import pap.mesus.controller.validator.CEP.CEPValidator;
import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.Endereco;
import pap.mesus.model.domain.Papel;
import pap.mesus.model.domain.PessoaFisica;
import pap.mesus.model.domain.PessoaJuridica;
import pap.mesus.model.domain.Reserva;
import pap.mesus.model.domain.Status;
import pap.mesus.model.domain.Usuario;

@Controller
public class ReceberDoacaoController {

	private static final String URL_CNPJ_WS = "http://www.receitaws.com.br/v1/cnpj/%s";
	private static final String JSON_NAT_JUR = "natureza_juridica";
	private static final String IFRAME_RECEBER = "/area/receberDoacao";

	private static final String RECEBER = "receber";

	private static final String PAPEL_RECEPTOR = "PAPEL_RECEPTOR";

	private static final String MAPA_DOACOES = "mapaDoacoes";

	private static final String CRUD_RECEPTOR = "crudReceptor";
	private static final Object EMPRESA_PUBLICA = "201-1";
	// private static final String MAPA_DOACOES_JSP = "mapaDoacoes";
	private static final String JSON_NOME = "nome";
	private static final String JSON_FANTASIA = "fantasia";
	private static final String JSON_CEP = "jsonCep";

	private static final String JSON_BAIRRO = "bairro";
	private static final String JSON_CIDADE = "localidade";
	private static final String JSON_ESTADO = "uf";
	private static final String JSON_RUA = "logradouro";
	private static final String MARCADORES_MAPA = "/area/doadores";
	private static final String JSON_LATITUDE = "latitude";
	private static final String JSON_LONGITUDE = "longitude";
	private static final String JSON_GRAMAS = "gramas";
	private static final String JSON_GRUPO_ALIMENTAR = "grupoAlimentar";
	private static final String JSON_LISTA_DOACOES = "listaDoacoes";
	private static final String RESERVAS = "reservas";
	private static final String LISTA_RESERVAS = "listaReservas";
	private static final String IFRAME_LISTA_RESERVAS = "/area/listaReservas";
	private static final String JSON_ENDERECO = "endereco";

	@Autowired
	PrimeiroPassoPessoaJuridica primeiroPassoPessoaJuridica;

	@Autowired
	private IPapelService papelService;

	@Autowired
	private IPessoaFisicaService pessoaFisicaService;

	@Autowired
	private IStatusService statusService;

	@Autowired
	private IReservaService reservaService;

	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IDoacaoService doacaoService;

	@Autowired
	IPessoaJuridicaService pessoaJuridicaService;

	@RequestMapping(value = { IFRAME_LISTA_RESERVAS })
	public ModelAndView iframeListarDoacao(ModelMap modelMap, HttpSession httpSession) {
		Usuario u = (Usuario) httpSession.getAttribute(PrincipalController.DADOS_USUARIO);
		PessoaJuridica pj = pessoaJuridicaService.responsavel(u);

		modelMap.addAttribute(RESERVAS, reservaService.listar(pj));
		return new ModelAndView(LISTA_RESERVAS);
	}

	@RequestMapping(value = { IFRAME_RECEBER })
	public ModelAndView iframeReceber(ModelMap modelMap, HttpSession httpSession) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			for (GrantedAuthority g : userDetail.getAuthorities()) {
				if (g.getAuthority().equals(PAPEL_RECEPTOR))
					return new ModelAndView(MAPA_DOACOES);
			}
		}
		modelMap.addAttribute(CRUD_RECEPTOR, primeiroPassoPessoaJuridica);
		return new ModelAndView(RECEBER);
	}

	@RequestMapping(value = "/area/cnpj", method = RequestMethod.GET, produces = "text/plain")
	@ResponseBody
	public String jsonCNPJ(@RequestParam(required = true) String numero, HttpSession httpSession) {
		boolean situacao = false;
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(String.format(URL_CNPJ_WS, numero)).get().build();

		try {
			Response response = client.newCall(request).execute();
			String dados = response.body().string();
			JSONObject j = new JSONObject(dados);
			String[] detNatJur = j.optString(JSON_NAT_JUR).split(" ");
			situacao = detNatJur[0].equals(EMPRESA_PUBLICA);
			httpSession.setAttribute(JSON_NOME, j.opt(JSON_NOME));
			httpSession.setAttribute(JSON_FANTASIA, j.opt(JSON_FANTASIA));

		} catch (Exception e) {

			e.printStackTrace();
		}
		return String.valueOf(situacao);

	}

	@RequestMapping(MARCADORES_MAPA)
	@ResponseBody
	public String dadosMapa(ModelMap modelMap, HttpSession httpSession) {

		JSONObject json = new JSONObject();

		Usuario u = (Usuario) httpSession.getAttribute(PrincipalController.DADOS_USUARIO);
		PessoaJuridica pj = pessoaJuridicaService.responsavel(u);
		List<Reserva> reservas = reservaService.listar(pj);
		try {
			json.putOpt(JSON_LATITUDE, pj.getEndereco().getLatitude());
			json.putOpt(JSON_LONGITUDE, pj.getEndereco().getLongitude());
			JSONArray ja = new JSONArray();
			for (Reserva r : reservas) {
				if (!r.isRejeitada() && r.getPessoaJuridica().equals(pj)) {
					JSONObject cada = new JSONObject();
					Doacao dr = r.getDoacao();
					Usuario dor = dr.getUsuario();
					PessoaFisica pf = pessoaFisicaService.usuario(dor);
					Endereco er = pf.getEndereco();
					cada.putOpt(JSON_LATITUDE, er.getLatitude());
					cada.putOpt(JSON_LONGITUDE, er.getLongitude());

					String montaEndereco = er.getLogradouro();
					if (er.getNumero() != null && !er.getNumero().isEmpty())
						montaEndereco += ", " + er.getNumero();
					if (er.getComplemento() != null && !er.getComplemento().isEmpty())
						montaEndereco += ", " + er.getComplemento();
					if (er.getBairro() != null && !er.getBairro().isEmpty())
						montaEndereco += ", Bairro " + er.getBairro();
					if (er.getCEP() != null && !er.getCEP().isEmpty())
						montaEndereco += ", CEP " + er.getCEP();
					if (er.getCidade() != null && !er.getCidade().isEmpty())
						montaEndereco += ", " + er.getCidade() + "-" + er.getEstado();

					cada.putOpt(JSON_ENDERECO, montaEndereco);
					cada.putOpt(JSON_GRAMAS, dr.getGramas());
					cada.putOpt(JSON_NOME, dor.getNome());
					cada.putOpt(JSON_GRUPO_ALIMENTAR, dr.getGrupoAlimentar().getNome());
					ja.put(cada);
				}
			}
			json.putOpt(JSON_LISTA_DOACOES, ja);
		} catch (JSONException e) {
			return null;
		}
		return json.toString();
	}

	@SuppressWarnings("serial")
	@RequestMapping(value = { "/area/receptor" }, method = RequestMethod.POST)
	public ModelAndView emailCep(
			@Valid @ModelAttribute(CRUD_RECEPTOR) PrimeiroPassoPessoaJuridica primeiroPassoPessoaJuridica,
			BindingResult bindingResult, ModelMap modelMap, HttpSession httpSession) {
		if (bindingResult.hasErrors()) {
			modelMap.addAttribute(CRUD_RECEPTOR, primeiroPassoPessoaJuridica);
			return new ModelAndView(RECEBER);
		}
		PessoaJuridica pj = new PessoaJuridica();
		Endereco e = new Endereco();
		JSONObject jsonCEP = (JSONObject) httpSession.getAttribute(JSON_CEP);
		e.setCEP(primeiroPassoPessoaJuridica.getCep());
		e.setBairro(jsonCEP.optString(JSON_BAIRRO));
		e.setCidade(jsonCEP.optString(JSON_CIDADE));
		e.setEstado(jsonCEP.optString(JSON_ESTADO));
		e.setComplemento(primeiroPassoPessoaJuridica.getComplemento());
		e.setNumero(primeiroPassoPessoaJuridica.getNumero());
		e.setLogradouro(jsonCEP.optString(JSON_RUA));
		GoogleMapsMesus.latLngEndereco(e);
		pj.setCnpj(primeiroPassoPessoaJuridica.getCnpj());
		Usuario u = (Usuario) httpSession.getAttribute(PrincipalController.DADOS_USUARIO);
		u.setUsuarioPapeis(new HashSet<Papel>() {
			{
				add(papelService.papelUsuario());
				add(papelService.papelReceptor());
			}
		});
		pj.setUsuario(u);
		pj.setEndereco(e);
		pj.setTelefone(primeiroPassoPessoaJuridica.getTelefone());
		pj.setNomeFantasia((String) httpSession.getAttribute(JSON_FANTASIA));
		pj.setNome((String) httpSession.getAttribute(JSON_NOME));
		pessoaJuridicaService.salvar(pj);

		// SecurityContext sec = SecurityContextHolder.getContext();
		// Authentication auth = sec.getAuthentication();
		//
		// List<GrantedAuthority> authorities = new
		// ArrayList<>(auth.getAuthorities());
		// authorities.add(new
		// SimpleGrantedAuthority(papelService.papelReceptor().getTipo()));
		// Authentication newAuth = new
		// UsernamePasswordAuthenticationToken(auth.getPrincipal(),
		// auth.getCredentials(),
		// authorities);
		//
		// sec.setAuthentication(newAuth);
		//
		// httpSession.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
		// sec);

		/**
		 * Authentication auth =
		 * SecurityContextHolder.getContext().getAuthentication(); if (auth !=
		 * null) { new SecurityContextLogoutHandler().logout(request, response,
		 * auth); } return "redirect:/login?logout";
		 */

		// return new ModelAndView(MAPA_DOACOES_JSP);
		return new ModelAndView("redirect:/login?logout");

	}

	@RequestMapping(value = "/area/buscacep", method = RequestMethod.GET, produces = "text/plain")
	@ResponseBody
	public String jsonCEP(@RequestParam(required = true) String numero, HttpSession httpSession) {
		JSONObject jsonCEP = CEPValidator.pesquisa(numero);
		httpSession.setAttribute(JSON_CEP, jsonCEP);
		return jsonCEP.toString();

	}

	@RequestMapping(value = "/area/reservar", method = RequestMethod.POST, produces = "text/plain")
	@ResponseBody
	public String reservarDoacao(@RequestParam(required = true) Integer n, HttpSession httpSession) {
		try {
			Usuario u = (Usuario) httpSession.getAttribute(PrincipalController.DADOS_USUARIO);
			PessoaJuridica pj = pessoaJuridicaService.responsavel(u);
			Doacao d = doacaoService.buscarPorId(n);
			Reserva r = new Reserva();
			r.setDoacao(d);
			r.setPessoaJuridica(pj);
			r.setData(new Date());
			reservaService.salvar(r);
			return String.valueOf(true);
		} catch (Exception e) {
			e.printStackTrace();
			return String.valueOf(false);
		}

	}

	@RequestMapping(value = "/area/buscacnpjusuario", method = RequestMethod.GET, produces = "text/plain")
	@ResponseBody
	public String jsonCNPJUsuario(@RequestParam(required = true) String numero) {
		return String.valueOf(pessoaJuridicaService.existeCNPJ(numero));

	}

	@RequestMapping(value = "/area/cancelarReserva", method = RequestMethod.GET, produces = "text/plain")
	public ModelAndView cancelarReserva(@RequestParam(required = true) Integer n, HttpSession httpSession,
			ModelMap modelMap) {
		try {
			Reserva r = reservaService.buscarPorId(n);
			Doacao d = atualizarStatusDoacao(r, statusService.cancelado());
			doacaoService.salvar(d);
			reservaService.apagar(r);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iframeListarDoacao(modelMap, httpSession);
	}

	@RequestMapping(value = "/area/confirmarReserva", method = RequestMethod.GET, produces = "text/plain")
	public ModelAndView confirmarReserva(@RequestParam(required = true) Integer n, HttpSession httpSession,
			ModelMap modelMap) {
		try {
			Reserva r = reservaService.buscarPorId(n);
			Doacao d = atualizarStatusDoacao(r, statusService.emTransporte());
			doacaoService.salvar(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iframeListarDoacao(modelMap, httpSession);
	}

	@RequestMapping(value = "/area/concluirReserva", method = RequestMethod.GET, produces = "text/plain")
	public ModelAndView concluirReserva(@RequestParam(required = true) Integer n, HttpSession httpSession,
			ModelMap modelMap) {
		try {
			Reserva r = reservaService.buscarPorId(n);
			Doacao d = atualizarStatusDoacao(r, statusService.concluido());
			doacaoService.salvar(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iframeListarDoacao(modelMap, httpSession);
	}

	private Doacao atualizarStatusDoacao(Reserva r, Status s) {
		Doacao d = r.getDoacao();
		d.setStatus(s);
		return d;

	}
}
