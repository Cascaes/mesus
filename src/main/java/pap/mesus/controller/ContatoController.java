package pap.mesus.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import pap.mesus.comunication.EnvioEmail;
import pap.mesus.model.domain.Usuario;

@Controller
public class ContatoController {
	private static final String IFRAME_CONTATO = "/area/contato";
	private static final String JSP_CONTATO = "contato";
	private static final String AJAX_FALE_CONOSCO = "/area/faleconosco";

	@Autowired
	EnvioEmail envioEmail;

	@RequestMapping(value = { IFRAME_CONTATO })
	public ModelAndView iframeListarDoacao(ModelMap modelMap, HttpSession httpSession) {
		return new ModelAndView(JSP_CONTATO);
	}

	@RequestMapping(value = AJAX_FALE_CONOSCO, method = RequestMethod.POST)
	@ResponseBody
	public String enviarContato(@RequestParam String assunto, @RequestParam String mensagem, HttpSession httpSession) {
		try {
			Usuario u = (Usuario) httpSession.getAttribute(PrincipalController.DADOS_USUARIO);
			String de = u.getEmail();
			envioEmail.contato(de, assunto, mensagem);
			return String.valueOf(true);
		} catch (Exception e) {
			e.printStackTrace();
			return String.valueOf(false);
		}

	}
}
