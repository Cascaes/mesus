package pap.mesus.controller.service;

import java.util.List;

public interface IService<T> {
	void salvar(T t);

	List<T> listar();

	void apagar(T t);

	T buscarPorId(Integer id);

	void atualizar(T t);

}
