package pap.mesus.controller.service;

import pap.mesus.model.domain.Status;

public interface IStatusService extends IService<Status> {
	Status statusInicial();

	Status expirado();

	Status concluido();

	Status cancelado();

	Status processo();

	Status emTransporte();

}
