package pap.mesus.controller.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pap.mesus.model.domain.Papel;
import pap.mesus.model.domain.Usuario;

@Service("autenticacaoService")
public class AutenticacaoService implements UserDetailsService {
	private static final String EMAIL_PERDIDO = "E-mail não encontrado";
	@Autowired
	private IUsuarioService usuarioService;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioService.buscarPorEmail(email);
		if (usuario == null) {
			throw new UsernameNotFoundException(EMAIL_PERDIDO);
		}
		return new org.springframework.security.core.userdetails.User(usuario.getEmail(), usuario.getSenha(),
				usuario.getAtivo(), true, true, true, getGrantedAuthorities(usuario));
	}

	private List<GrantedAuthority> getGrantedAuthorities(Usuario user) {
		List<GrantedAuthority> authorities = new ArrayList<>();

		for (Papel papelUsuario : user.getUsuarioPapeis()) {
			authorities.add(new SimpleGrantedAuthority(papelUsuario.getTipo()));
		}
		return authorities;
	}

}
