package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Alimento;
import pap.mesus.model.domain.GrupoAlimentar;

@Service("alimentoService")
@Transactional
public class AlimentoService implements IAlimentoService {

	private static final String GRUPO_ALIMENTAR_ID = "grupoAlimentar";
	@Autowired
	private DAO<Alimento> alimentoDAO;

	@Override
	public void salvar(Alimento t) {

	}

	@Override
	public List<Alimento> listar() {
		return alimentoDAO.listar();
	}

	@Override
	public void apagar(Alimento t) {
	}

	@Override
	public Alimento buscarPorId(Integer id) {
		return alimentoDAO.selecionar(id);
	}

	@Override
	public void atualizar(Alimento t) {

	}

	@Override
	public List<Alimento> listar(GrupoAlimentar id) {
		return alimentoDAO.listarSelecionado(GRUPO_ALIMENTAR_ID, id);
	}

}
