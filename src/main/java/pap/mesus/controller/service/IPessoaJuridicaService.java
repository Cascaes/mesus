package pap.mesus.controller.service;

import pap.mesus.model.domain.PessoaJuridica;
import pap.mesus.model.domain.Usuario;

public interface IPessoaJuridicaService extends IService<PessoaJuridica> {
	boolean existeCNPJ(String cnpj);

	PessoaJuridica responsavel(Usuario u);
}
