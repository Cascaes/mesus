package pap.mesus.controller.service;

import java.util.List;

import pap.mesus.model.domain.view.DadosDoacoes;

public interface IDadosDoacoesService extends IService<DadosDoacoes> {
	List<DadosDoacoes> listaRotina();

}
