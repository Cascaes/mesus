package pap.mesus.controller.service;

import java.util.List;

import pap.mesus.model.domain.Alimento;
import pap.mesus.model.domain.GrupoAlimentar;

public interface IAlimentoService extends IService<Alimento> {
	List<Alimento> listar(GrupoAlimentar id);
}
