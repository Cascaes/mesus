package pap.mesus.controller.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.Usuario;

@Service("doacaoService")
@Transactional
public class DoacaoService implements IDoacaoService {

	private static final String USUARIO = "usuario";
	@Autowired
	private DAO<Doacao> doacaoDAO;

	@Override
	public void salvar(Doacao t) {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
		t.setIdentificador(Long.parseLong(sdf.format(d)));
		doacaoDAO.inserir(t);

	}

	@Override
	public List<Doacao> listar() {
		return doacaoDAO.listar();
	}

	@Override
	public void apagar(Doacao t) {
		doacaoDAO.apagar(t);
	}

	@Override
	public Doacao buscarPorId(Integer id) {
		return doacaoDAO.selecionar(id);
	}

	@Override
	public void atualizar(Doacao t) {
		doacaoDAO.atualizar(t);

	}

	@Override
	public List<Doacao> listar(Usuario usuario) {
		return doacaoDAO.listarSelecionado(USUARIO, usuario);
	}

	@Override
	public void apagarPorId(Integer id) {
		doacaoDAO.apagarPeloId(id);

	}

}
