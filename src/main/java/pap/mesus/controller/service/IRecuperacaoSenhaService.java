package pap.mesus.controller.service;

import pap.mesus.model.domain.RecuperacaoSenha;

public interface IRecuperacaoSenhaService extends IService<RecuperacaoSenha> {
	RecuperacaoSenha buscarPorToken(String token);
}
