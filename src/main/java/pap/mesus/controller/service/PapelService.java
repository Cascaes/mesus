package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Papel;

@Service("papelService")
@Transactional
public class PapelService implements IPapelService {
	@Autowired
	private DAO<Papel> papelDAO;

	@Override
	public void salvar(Papel t) {

	}

	@Override
	public Papel papelUsuario() {
		List<Papel> lista = listar();
		for (Papel papel : lista)
			if (papel.getTipo().equals("PAPEL_USUARIO"))
				return papel;
		return null;

	}

	@Override
	public List<Papel> listar() {
		return papelDAO.listar();
	}

	@Override
	public void apagar(Papel t) {

	}

	@Override
	public Papel buscarPorId(Integer id) {
		return null;
	}

	@Override
	public void atualizar(Papel t) {

	}

	@Override
	public Papel papelReceptor() {
		List<Papel> lista = listar();
		for (Papel papel : lista)
			if (papel.getTipo().equals("PAPEL_RECEPTOR"))
				return papel;
		return null;
	}

}
