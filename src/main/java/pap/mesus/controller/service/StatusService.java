package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Status;

@Service("statusService")
@Transactional
public class StatusService implements IStatusService {

	@Autowired
	private DAO<Status> statusDAO;

	@Override
	public void salvar(Status t) {
		statusDAO.inserir(t);

	}

	@Override
	public List<Status> listar() {
		return statusDAO.listar();
	}

	@Override
	public void apagar(Status t) {
		statusDAO.apagar(t);
	}

	@Override
	public Status buscarPorId(Integer id) {
		return statusDAO.selecionar(id);
	}

	@Override
	public void atualizar(Status t) {
		statusDAO.atualizar(t);
	}

	public enum Statuses {
		ABERTO, // ao criar doação
		PROCESSO, // ao sistema realizar procedimento de reserva de doação
		CANCELADO, // ao dono da doação cancelar
		EXPIRADO, // ao acabar prazo da doação
		CONCLUIDO, // ao dono da doação confirmar busca do receptor
		TRANSPORTE; // ao receptor confirmar ida à busca da doação
	}

	@Override
	public Status statusInicial() {
		return statusDAO.selecionarPeloCampo("rotulo", Statuses.ABERTO.name());
	}

	@Override
	public Status expirado() {
		return statusDAO.selecionarPeloCampo("rotulo", Statuses.EXPIRADO.name());
	}

	@Override
	public Status concluido() {
		return statusDAO.selecionarPeloCampo("rotulo", Statuses.CONCLUIDO.name());
	}

	@Override
	public Status cancelado() {
		return statusDAO.selecionarPeloCampo("rotulo", Statuses.CANCELADO.name());
	}

	@Override
	public Status processo() {
		return statusDAO.selecionarPeloCampo("rotulo", Statuses.PROCESSO.name());
	}

	@Override
	public Status emTransporte() {
		return statusDAO.selecionarPeloCampo("rotulo", Statuses.TRANSPORTE.name());
	}

}
