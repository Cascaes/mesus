package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.GrupoAlimentar;

@Service("grupoAlimentarService")
@Transactional
public class GrupoAlimentarService implements IService<GrupoAlimentar> {

	@Autowired
	private DAO<GrupoAlimentar> grupoAlimentarDAO;

	@Override
	public void salvar(GrupoAlimentar t) {

	}

	@Override
	public List<GrupoAlimentar> listar() {
		return grupoAlimentarDAO.listar();
	}

	@Override
	public void apagar(GrupoAlimentar t) {
	}

	@Override
	public GrupoAlimentar buscarPorId(Integer id) {
		return grupoAlimentarDAO.selecionar(id);
	}

	@Override
	public void atualizar(GrupoAlimentar t) {

	}

}
