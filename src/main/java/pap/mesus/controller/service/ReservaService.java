package pap.mesus.controller.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.PessoaJuridica;
import pap.mesus.model.domain.Reserva;
import pap.mesus.model.domain.ReservaComparator;

@Service("reservaService")
@Transactional
public class ReservaService implements IReservaService {
	@Autowired
	private DAO<Reserva> reservaDAO;

	@Override
	public void salvar(Reserva t) {
		reservaDAO.inserir(t);

	}

	@Override
	public List<Reserva> listar() {
		return reservaDAO.listar();
	}

	@Override
	public void apagar(Reserva t) {
		reservaDAO.apagar(t);
	}

	@Override
	public Reserva buscarPorId(Integer id) {
		return reservaDAO.selecionar(id);
	}

	@Override
	public void atualizar(Reserva t) {
		reservaDAO.atualizar(t);

	}

	@Override
	public List<Reserva> listar(PessoaJuridica pj) {
		List<Reserva> res = listar();
		List<Reserva> ress = new ArrayList<>();
		for (Reserva r : res) {
			if (r.getPessoaJuridica().getId() == pj.getId())
				ress.add(r);
		}
		return ress;
	}

	@Override
	public Reserva ultimaReserva(PessoaJuridica pj) {
		List<Reserva> res = listar();
		Collections.sort(res, new ReservaComparator());
		Reserva r = res.get(res.size() - 1);
		return r;
	}

	@Override
	public Reserva qual(Doacao doacao) {
		return reservaDAO.selecionarPeloCampo("doacao", doacao);
	}

}
