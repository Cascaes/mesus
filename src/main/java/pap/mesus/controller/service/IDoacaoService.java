package pap.mesus.controller.service;

import java.util.List;

import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.Usuario;

public interface IDoacaoService extends IService<Doacao> {
	List<Doacao> listar(Usuario usuario);

	void apagarPorId(Integer id);

}
