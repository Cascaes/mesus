package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.PessoaFisica;
import pap.mesus.model.domain.Usuario;

@Service("pessoaFisicaService")
@Transactional
public class PessoaFisicaService implements IPessoaFisicaService {

	@Autowired
	private DAO<PessoaFisica> pessoaFisicaDAO;

	@Override
	public void salvar(PessoaFisica t) {
		pessoaFisicaDAO.inserir(t);

	}

	@Override
	public List<PessoaFisica> listar() {
		return pessoaFisicaDAO.listar();
	}

	@Override
	public void apagar(PessoaFisica t) {
		pessoaFisicaDAO.apagar(t);
	}

	@Override
	public PessoaFisica buscarPorId(Integer id) {
		return pessoaFisicaDAO.selecionar(id);
	}

	@Override
	public void atualizar(PessoaFisica t) {
		pessoaFisicaDAO.atualizar(t);

	}

	@Override
	public PessoaFisica buscarPorCPF(String cpf) {
		return pessoaFisicaDAO.selecionarPeloCampo(PessoaFisica.COLUNA_CPF.toLowerCase(), cpf);
	}

	@Override
	public boolean existeCPF(String cpf) {
		return pessoaFisicaDAO.dadoExiste(PessoaFisica.COLUNA_CPF.toLowerCase(), cpf);
	}

	@Override
	public PessoaFisica usuario(Usuario usuario) {
		return pessoaFisicaDAO.selecionarPeloCampo("usuario", usuario);
	}

}
