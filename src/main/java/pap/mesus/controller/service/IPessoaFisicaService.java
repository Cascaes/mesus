package pap.mesus.controller.service;

import pap.mesus.model.domain.PessoaFisica;
import pap.mesus.model.domain.Usuario;

public interface IPessoaFisicaService extends IService<PessoaFisica> {
	PessoaFisica buscarPorCPF(String cpf);

	PessoaFisica usuario(Usuario usuario);

	boolean existeCPF(String cpf);
}
