package pap.mesus.controller.service;

import pap.mesus.model.domain.Usuario;

public interface IUsuarioService extends IService<Usuario> {
	boolean existeEmail(String email);

	Usuario buscarPorEmail(String email);

}
