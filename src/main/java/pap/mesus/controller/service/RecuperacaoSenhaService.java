package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.RecuperacaoSenha;

@Service("recuperacaoSenhaService")
@Transactional
public class RecuperacaoSenhaService implements IRecuperacaoSenhaService {

	@Autowired
	private DAO<RecuperacaoSenha> recuperacaoSenhaDAO;

	@Override
	public void salvar(RecuperacaoSenha t) {
		recuperacaoSenhaDAO.inserir(t);

	}

	@Override
	public List<RecuperacaoSenha> listar() {
		return recuperacaoSenhaDAO.listar();
	}

	@Override
	public void apagar(RecuperacaoSenha t) {
		recuperacaoSenhaDAO.apagar(t);

	}

	@Override
	public RecuperacaoSenha buscarPorId(Integer id) {
		return recuperacaoSenhaDAO.selecionar(id);
	}

	@Override
	public void atualizar(RecuperacaoSenha t) {
		recuperacaoSenhaDAO.atualizar(t);

	}

	@Override
	public RecuperacaoSenha buscarPorToken(String token) {
		return recuperacaoSenhaDAO.selecionarPeloCampo("token", token);
	}

}
