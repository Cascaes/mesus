package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.IDadosDoacoesDAO;
import pap.mesus.model.domain.view.DadosDoacoes;

@Service("dadosDoacoesService")
@Transactional
public class DadosDoacoesService implements IDadosDoacoesService {
	@Autowired
	private IDadosDoacoesDAO dadosDoacoesDAO;

	@Override
	public void salvar(DadosDoacoes t) {

	}

	@Override
	public List<DadosDoacoes> listar() {
		return dadosDoacoesDAO.listar();
	}

	@Override
	public void apagar(DadosDoacoes t) {

	}

	@Override
	public DadosDoacoes buscarPorId(Integer id) {
		return dadosDoacoesDAO.selecionar(id);
	}

	@Override
	public void atualizar(DadosDoacoes t) {

	}

	@Override
	public List<DadosDoacoes> listaRotina() {
		return dadosDoacoesDAO.listarRotina();
	}

}
