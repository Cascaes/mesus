package pap.mesus.controller.service;

import java.util.List;

import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.PessoaJuridica;
import pap.mesus.model.domain.Reserva;

public interface IReservaService extends IService<Reserva> {
	List<Reserva> listar(PessoaJuridica pj);

	Reserva qual(Doacao doacao);

	Reserva ultimaReserva(PessoaJuridica pj);
}
