package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Endereco;

@Service("enderecoService")
@Transactional
public class EnderecoService implements IService<Endereco> {

	@Autowired
	private DAO<Endereco> enderecoDAO;

	@Override
	public void salvar(Endereco t) {
		enderecoDAO.inserir(t);

	}

	@Override
	public List<Endereco> listar() {
		return enderecoDAO.listar();
	}

	@Override
	public void apagar(Endereco t) {
		enderecoDAO.apagar(t);
	}

	@Override
	public Endereco buscarPorId(Integer id) {
		return enderecoDAO.selecionar(id);
	}

	@Override
	public void atualizar(Endereco t) {
		enderecoDAO.atualizar(t);
	}

}
