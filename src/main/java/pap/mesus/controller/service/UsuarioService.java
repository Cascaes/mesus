package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.Usuario;

@Service("usuarioService")
@Transactional
public class UsuarioService implements IUsuarioService {

	@Autowired
	private DAO<Usuario> usuarioDAO;

	@Override
	public void salvar(Usuario t) {
		usuarioDAO.inserir(t);

	}

	@Override
	public List<Usuario> listar() {
		return usuarioDAO.listar();
	}

	@Override
	public void apagar(Usuario t) {
		usuarioDAO.apagar(t);

	}

	@Override
	public Usuario buscarPorId(Integer id) {
		return usuarioDAO.selecionar(id);
	}

	@Override
	public Usuario buscarPorEmail(String email) {
		return usuarioDAO.selecionarPeloCampo(Usuario.COLUNA_EMAIL.toLowerCase(), email);
	}

	@Override
	public void atualizar(Usuario t) {
		usuarioDAO.atualizar(t);

	}

	@Override
	public boolean existeEmail(String email) {
		return usuarioDAO.dadoExiste(Usuario.COLUNA_EMAIL.toLowerCase(), email);
	}

}
