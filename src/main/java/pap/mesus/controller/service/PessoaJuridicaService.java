package pap.mesus.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pap.mesus.model.dao.DAO;
import pap.mesus.model.domain.PessoaJuridica;
import pap.mesus.model.domain.Usuario;

@Service("pessoaJuridicaService")
@Transactional
public class PessoaJuridicaService implements IPessoaJuridicaService {
	@Autowired
	private DAO<PessoaJuridica> pessoaJuridicalDAO;

	@Override
	public void salvar(PessoaJuridica t) {
		pessoaJuridicalDAO.inserir(t);
	}

	@Override
	public List<PessoaJuridica> listar() {
		return pessoaJuridicalDAO.listar();
	}

	@Override
	public void apagar(PessoaJuridica t) {

	}

	@Override
	public PessoaJuridica buscarPorId(Integer id) {
		return null;
	}

	@Override
	public void atualizar(PessoaJuridica t) {
		pessoaJuridicalDAO.atualizar(t);
	}

	@Override
	public boolean existeCNPJ(String cnpj) {
		return pessoaJuridicalDAO.dadoExiste(PessoaJuridica.COLUNA_CNPJ.toLowerCase(), cnpj);
	}

	@Override
	public PessoaJuridica responsavel(Usuario u) {

		return pessoaJuridicalDAO.selecionarPeloCampo("usuario", u);
	}

}
