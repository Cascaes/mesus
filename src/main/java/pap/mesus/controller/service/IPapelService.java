package pap.mesus.controller.service;

import pap.mesus.model.domain.Papel;

public interface IPapelService extends IService<Papel> {
	Papel papelUsuario();

	Papel papelReceptor();
}
