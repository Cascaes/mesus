package pap.mesus.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import atg.taglib.json.util.JSONObject;
import pap.mesus.controller.service.IPessoaFisicaService;
import pap.mesus.controller.service.IUsuarioService;
import pap.mesus.controller.validator.PrimeiroPassoUsuario;
import pap.mesus.controller.validator.SegundoPassoUsuario;
import pap.mesus.controller.validator.TerceiroPassoUsuario;
import pap.mesus.controller.validator.CEP.CEPValidator;

@Controller
public class PassoAPassoController {

	@Autowired
	ServletContext context;

	@Autowired
	SegundoPassoUsuario segundoPassoUsuario;

	@Autowired
	PrimeiroPassoUsuario primeiroPassoUsuario;

	@Autowired
	TerceiroPassoUsuario terceiroPassoUsuario;

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private IPessoaFisicaService pessoaFisicaService;

	private static final String EMAIL_CEP = "/email_cep";

	private static final String SEGUNDO_PASSO_JSP = "segundo_passo";

	private static final String CADASTRANDO = "/cadastro_geral";

	public static final String SEGUNDO_PASSO = "segundoPasso";

	public static final String JSON_CEP_RUA = "logradouro";

	public static final String JSON_CEP_BAIRRO = "bairro";

	public static final String JSON_CEP_CIDADE = "localidade";

	public static final String JSON_CEP_ESTADO = "uf";

	private static final String TERCEIRO_PASSO_JSP = "terceiro_passo";

	private static final String TERCEIRO_PASSO = "terceiroPasso";

	public static final String CEP = "dadosCep";

	public static final String CONTA = "conta";

	private static final String MINHA_CONTA = "minhaconta";

	public static final String PRIMEIRO_PASSO = "primeiroPasso";
	private JSONObject cep;

	@RequestMapping(value = { MINHA_CONTA })
	public ModelAndView conta(ModelMap modelMap) {
		modelMap.addAttribute(PRIMEIRO_PASSO, primeiroPassoUsuario);
		return new ModelAndView(CONTA);
	}

	@RequestMapping(value = { EMAIL_CEP }, method = RequestMethod.POST)
	public ModelAndView emailCep(@Valid @ModelAttribute(PRIMEIRO_PASSO) PrimeiroPassoUsuario primeiroPassoUsuario,
			BindingResult bindingResult, ModelMap modelMap, HttpSession httpSession) {
		if (usuarioService.existeEmail(primeiroPassoUsuario.getEmail())) {
			bindingResult.rejectValue(PrimeiroPassoUsuario.ErrorEmailExists.CAMPO_EMAIL, PRIMEIRO_PASSO,
					PrimeiroPassoUsuario.ErrorEmailExists.MENSAGEM_EMAIL);
		}
		if (!bindingResult.hasFieldErrors(PrimeiroPassoUsuario.ErrorEmailExists.CAMPO_CEP)) {
			cep = CEPValidator.pesquisa(primeiroPassoUsuario.getCEP());
			if (cep.optBoolean(CEPValidator.JSON_ERRO)) {
				bindingResult.rejectValue(PrimeiroPassoUsuario.ErrorEmailExists.CAMPO_CEP, PRIMEIRO_PASSO,
						PrimeiroPassoUsuario.ErrorEmailExists.MENSAGEM_CEP);
			} else {
				dadosCep(modelMap);
			}
		}

		modelMap.addAttribute(SEGUNDO_PASSO, segundoPassoUsuario);
		httpSession.setAttribute(PRIMEIRO_PASSO, primeiroPassoUsuario);
		httpSession.setAttribute(CEP, cep);
		modelMap.addAttribute(PRIMEIRO_PASSO, primeiroPassoUsuario);
		if (bindingResult.hasErrors()) {
			return new ModelAndView(CONTA);
		}

		return new ModelAndView(SEGUNDO_PASSO_JSP);

	}

	private void dadosCep(final ModelMap modelMap) {
		modelMap.addAttribute(JSON_CEP_RUA, cep.optString(JSON_CEP_RUA));
		modelMap.addAttribute(JSON_CEP_BAIRRO, cep.optString(JSON_CEP_BAIRRO));
		modelMap.addAttribute(JSON_CEP_CIDADE, cep.optString(JSON_CEP_CIDADE));
		modelMap.addAttribute(JSON_CEP_ESTADO, cep.optString(JSON_CEP_ESTADO));
	}

	@RequestMapping(value = { CADASTRANDO }, method = RequestMethod.POST)
	public ModelAndView cadastroSenha(@Valid @ModelAttribute(SEGUNDO_PASSO) SegundoPassoUsuario segundoPassoUsuario,
			BindingResult bindingResult, ModelMap modelMap, HttpSession httpSession) {
		if (pessoaFisicaService.existeCPF(segundoPassoUsuario.getCpf())) {
			bindingResult.rejectValue(SegundoPassoUsuario.ErrorCPFExists.CAMPO_CPF, SEGUNDO_PASSO,
					SegundoPassoUsuario.ErrorCPFExists.MENSAGEM_CPF);
		}
		modelMap.addAttribute(PRIMEIRO_PASSO, httpSession.getAttribute(PRIMEIRO_PASSO));
		modelMap.addAttribute(SEGUNDO_PASSO, segundoPassoUsuario);
		dadosCep(modelMap);
		if (bindingResult.hasErrors()) {

			return new ModelAndView(SEGUNDO_PASSO_JSP);
		}
		httpSession.setAttribute(SEGUNDO_PASSO, segundoPassoUsuario);
		modelMap.addAttribute(TERCEIRO_PASSO, terceiroPassoUsuario);
		return new ModelAndView(TERCEIRO_PASSO_JSP);

	}

}
