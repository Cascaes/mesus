package pap.mesus.model.dao;

import java.util.List;

import pap.mesus.model.domain.view.DadosDoacoes;

public interface IDadosDoacoesDAO extends DAO<DadosDoacoes> {
	List<DadosDoacoes> listarRotina();

}
