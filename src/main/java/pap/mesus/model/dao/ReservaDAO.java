package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Reserva;

@Repository("reservaDAO")
public class ReservaDAO extends AbstractDAO<Reserva> {

	@Override
	public Class<Reserva> getTipo() {
		return Reserva.class;
	}

}
