package pap.mesus.model.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;

public abstract class AbstractDAO<T> implements DAO<T> {
	protected final String PARAM_ID = "IIDD";
	protected final String ID = "id";
	protected final Class<T> tipoClasse;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.tipoClasse = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractDAO.class);
	}

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void persist(Object entity) {
		getSession().merge(entity);
	}

	public void delete(Object entity) {
		getSession().delete(entity);
	}

	@Override
	public void inserir(T t) {
		persist(t);
	}

	@Override
	public void atualizar(T t) {
		getSession().update(t);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> listarSelecionado(String campo, Object dado) {
		Criteria c = getSession().createCriteria(getTipo());
		c.add(Restrictions.like(campo, dado));
		return c.list();
	}

	@Override
	public Boolean dadoExiste(String campo, String dado) {
		Criteria c = getSession().createCriteria(getTipo());
		c.add(Restrictions.like(campo, dado));
		return c.uniqueResult() != null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public T selecionarPeloCampo(String campo, Object dado) {
		Criteria c = getSession().createCriteria(getTipo());
		c.add(Restrictions.like(campo, dado));
		return (T) c.uniqueResult();
	}

	public abstract Class<T> getTipo();

	@SuppressWarnings("unchecked")
	@Override
	public T selecionar(Integer id) {

		Criteria criteria = getSession().createCriteria(getTipo());
		criteria.add(Restrictions.eq(ID, id));
		return (T) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> listar() {
		Criteria criteria = getSession().createCriteria(getTipo());
		return criteria.list();
	}

	@Override
	public void apagar(T t) {
		getSession().delete(t);
	}

	@Override
	public void apagarPeloId(Integer id) {

		Query query = getSession()
				.createQuery(String.format("delete from " + tipoClasse.getSimpleName() + " where id = :" + PARAM_ID));
		query.setInteger(PARAM_ID, id);
		query.executeUpdate();

	}
}