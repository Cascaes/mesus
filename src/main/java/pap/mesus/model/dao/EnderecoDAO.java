package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Endereco;

@Repository("enderecoDAO")
public class EnderecoDAO extends AbstractDAO<Endereco> {

	@Override
	public Class<Endereco> getTipo() {
		return Endereco.class;
	}

}
