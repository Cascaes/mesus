package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Doacao;

@Repository("doacaoDAO")
public class DoacaoDAO extends AbstractDAO<Doacao> {

	@Override
	public Class<Doacao> getTipo() {
		return Doacao.class;
	}

}
