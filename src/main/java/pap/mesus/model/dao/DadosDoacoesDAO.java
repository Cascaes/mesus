package pap.mesus.model.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pap.mesus.controller.service.StatusService;
import pap.mesus.model.domain.view.DadosDoacoes;

@Repository("dadosDoacoesDAO")
public class DadosDoacoesDAO implements IDadosDoacoesDAO {

	private static final String CAMPO_STATUS = "status";
	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();

	}

	@Override
	public void inserir(DadosDoacoes t) {

	}

	@Override
	public void atualizar(DadosDoacoes t) {

	}

	@Override
	public DadosDoacoes selecionar(Integer id) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DadosDoacoes> listar() {
		Criteria criteria = getSession().createCriteria(DadosDoacoes.class);
		return criteria.list();
	}

	@Override
	public List<DadosDoacoes> listarSelecionado(String campo, Object dado) {
		return null;
	}

	@Override
	public void apagar(DadosDoacoes t) {

	}

	@Override
	public void apagarPeloId(Integer id) {

	}

	@Override
	public Boolean dadoExiste(String campo, String dado) {
		return null;
	}

	@Override
	public DadosDoacoes selecionarPeloCampo(String campo, Object dado) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DadosDoacoes> listarRotina() {
		Criteria criteria = getSession().createCriteria(DadosDoacoes.class);
		Criterion statusAberto = Restrictions.eq(CAMPO_STATUS, StatusService.Statuses.ABERTO.name());
		Criterion statusProcesso = Restrictions.eq(CAMPO_STATUS, StatusService.Statuses.PROCESSO.name());
		criteria.add(Restrictions.or(statusAberto, statusProcesso));
		return criteria.list();
	}

}
