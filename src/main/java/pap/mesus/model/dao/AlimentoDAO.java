package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Alimento;

@Repository("alimentoDAO")
public class AlimentoDAO extends AbstractDAO<Alimento> {

	@Override
	public Class<Alimento> getTipo() {
		return Alimento.class;
	}

}
