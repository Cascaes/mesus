package pap.mesus.model.dao;

import java.util.List;

public interface DAO<T> {
	void inserir(T t);

	void atualizar(T t);

	T selecionar(Integer id);

	List<T> listar();

	List<T> listarSelecionado(String campo, Object dado);

	void apagar(T t);

	void apagarPeloId(Integer id);

	Boolean dadoExiste(String campo, String dado);

	T selecionarPeloCampo(String campo, Object dado);

}
