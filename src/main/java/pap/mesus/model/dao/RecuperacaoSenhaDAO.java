package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.RecuperacaoSenha;

@Repository("recuperacaoSenhaDAO")
public class RecuperacaoSenhaDAO extends AbstractDAO<RecuperacaoSenha> {

	@Override
	public Class<RecuperacaoSenha> getTipo() {
		return RecuperacaoSenha.class;
	}

}
