package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.PessoaJuridica;

@Repository("pessoaJuridicaDAO")
public class PessoaJuridicaDAO extends AbstractDAO<PessoaJuridica> {

	@Override
	public Class<PessoaJuridica> getTipo() {
		return PessoaJuridica.class;
	}

}
