package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Papel;

@Repository("papelDAO")
public class PapelDAO extends AbstractDAO<Papel> {

	@Override
	public Class<Papel> getTipo() {
		return Papel.class;
	}

}
