package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.GrupoAlimentar;

@Repository("grupoAlimentarDAO")
public class GrupoAlimentarDAO extends AbstractDAO<GrupoAlimentar> {

	@Override
	public Class<GrupoAlimentar> getTipo() {
		return GrupoAlimentar.class;
	}

}
