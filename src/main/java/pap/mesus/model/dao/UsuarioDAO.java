package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Usuario;

@Repository("usuarioDAO")
public class UsuarioDAO extends AbstractDAO<Usuario> {

	@Override
	public Class<Usuario> getTipo() {
		return Usuario.class;
	}

}
