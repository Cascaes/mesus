package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.PessoaFisica;

@Repository("pessoaFisicaDAO")
public class PessoaFisicaDAO extends AbstractDAO<PessoaFisica> {

	@Override
	public Class<PessoaFisica> getTipo() {
		return PessoaFisica.class;
	}

}
