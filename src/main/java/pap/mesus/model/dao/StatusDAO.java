package pap.mesus.model.dao;

import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.Status;

@Repository("statusDAO")
public class StatusDAO extends AbstractDAO<Status> {

	@Override
	public Class<Status> getTipo() {
		return Status.class;
	}

}
