package pap.mesus.model.domain.abstracts;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;

@MappedSuperclass
public abstract class CipherId {
	private static Cipher ecipher;
	private static Cipher dcipher;
	private static final String DES = "DES";
	private static final String UTF8 = "UTF8";
	private static SecretKey key;

	public abstract Integer getId();

	public CipherId() {
		try {
			key = KeyGenerator.getInstance("DES").generateKey();

			ecipher = Cipher.getInstance(DES);
			dcipher = Cipher.getInstance(DES);
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
	}

	@Transient
	public String idEncriptado() {
		byte[] utf8;
		byte[] enc;
		try {
			utf8 = String.valueOf(getId()).getBytes(UTF8);
			enc = ecipher.doFinal(utf8);
			enc = BASE64EncoderStream.encode(enc);
		} catch (Exception e) {
			return null;
		}
		return new String(enc);

	}

	@Transient
	public static String descriptar(String str) {

		try {
			byte[] dec = BASE64DecoderStream.decode(str.getBytes());
			byte[] utf8 = dcipher.doFinal(dec);
			return new String(utf8, "UTF8");
		} catch (Exception e) {
			return null;
		}

	}
}
