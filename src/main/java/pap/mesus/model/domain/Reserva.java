package pap.mesus.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "RESERVA")
public class Reserva implements Serializable {
	private static final long serialVersionUID = 5329958718888254570L;
	private static final String TABELA_RESERVA = "RESERVA";
	private static final String COLUNA_ID = "ID";
	public static final String COLUNA_DOACAO_ID = "DOACAO_ID";
	private static final String COLUNA_PJ_ID = "PESSOA_JURIDICA_ID";
	private static final String COLUNA_DATA = "DATA";
	private static final String COLUNA_REJEITADA = "REJEITADA";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_RESERVA, name = COLUNA_ID)
	private Integer id;

	@OneToOne
	@JoinColumn(table = TABELA_RESERVA, name = COLUNA_DOACAO_ID)
	private Doacao doacao;

	@ManyToOne
	@JoinColumn(table = TABELA_RESERVA, name = COLUNA_PJ_ID)
	private PessoaJuridica pessoaJuridica;

	@Column(table = TABELA_RESERVA, name = COLUNA_DATA)
	private Date data;

	@Column(table = TABELA_RESERVA, name = COLUNA_REJEITADA)
	private boolean rejeitada;

	@Transient
	public Integer getHorasPrazo() {
		Date agora = new Date();
		return (int) Math.floor(24 - ((double) (agora.getTime() - getData().getTime())) / (3600 * 1000));
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Doacao getDoacao() {
		return doacao;
	}

	public void setDoacao(Doacao doacao) {
		this.doacao = doacao;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public boolean isRejeitada() {
		return rejeitada;
	}

	public void setRejeitada(boolean rejeitada) {
		this.rejeitada = rejeitada;
	}

	@Override
	public String toString() {
		return "Reserva [id=" + id + ", doacao=" + doacao + ", pessoaJuridica=" + pessoaJuridica + ", data=" + data
				+ ", rejeitada=" + rejeitada + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserva other = (Reserva) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
