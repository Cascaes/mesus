package pap.mesus.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

@Entity
@Table(name = "GRUPO_ALIMENTAR")
@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GrupoAlimentar implements Serializable {
	private static final long serialVersionUID = 1371612659293267067L;
	private static final String TABELA_GRUPO_ALIMENTAR = "GRUPO_ALIMENTAR";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_VALIDADE = "VALIDADE";
	private static final String COLUNA_NOME = "NOME";
	private static final String COLUNA_PESO_BRUTO = "PESO_BRUTO";
	private static final String COLUNA_PORCOES = "PORCOES";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_GRUPO_ALIMENTAR, name = COLUNA_ID)
	private Integer id;

	@Column(table = TABELA_GRUPO_ALIMENTAR, name = COLUNA_NOME, nullable = false)
	private String nome;

	@Column(table = TABELA_GRUPO_ALIMENTAR, name = COLUNA_PESO_BRUTO, nullable = false)
	private Integer pesoBruto;

	@Column(table = TABELA_GRUPO_ALIMENTAR, name = COLUNA_VALIDADE, nullable = false)
	private Integer validade;

	@Column(table = TABELA_GRUPO_ALIMENTAR, name = COLUNA_PORCOES, nullable = false)
	private Integer porcoes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(Integer pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public Integer getPorcoes() {
		return porcoes;
	}

	public void setPorcoes(Integer porcoes) {
		this.porcoes = porcoes;
	}

	public Integer getValidade() {
		return validade;
	}

	public void setValidade(Integer validade) {
		this.validade = validade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoAlimentar other = (GrupoAlimentar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GrupoAlimentar [id=" + id + ", nome=" + nome + ", pesoBruto=" + pesoBruto + ", validade=" + validade
				+ ", porcoes=" + porcoes + "]";
	}

}
