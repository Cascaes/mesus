package pap.mesus.model.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

@Entity
@Table(name = "ALIMENTO")
@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Alimento implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String TABELA_ALIMENTO = "ALIMENTO";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_NOME = "NOME";
	private static final String COLUNA_PESO_BRUTO = "PESO_BRUTO";
	private static final String COLUNA_PESO_COZIDO = "PESO_COZIDO";
	private static final String COLUNA_GRUPO_ALIMENTAR = "GRUPO_ALIMENTAR_ID";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_ALIMENTO, name = COLUNA_ID)
	public Integer id;

	@Column(table = TABELA_ALIMENTO, name = COLUNA_NOME, nullable = false)
	public String nome;

	@Column(table = TABELA_ALIMENTO, name = COLUNA_PESO_COZIDO, nullable = false)
	public Integer pesoCozido;

	@Column(table = TABELA_ALIMENTO, name = COLUNA_PESO_BRUTO, nullable = false)
	public Integer pesoBruto;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_GRUPO_ALIMENTAR, nullable = false, insertable = true, updatable = true)
	public GrupoAlimentar grupoAlimentar;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPesoCozido() {
		return pesoCozido;
	}

	public void setPesoCozido(Integer pesoCozido) {
		this.pesoCozido = pesoCozido;
	}

	public Integer getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(Integer pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public GrupoAlimentar getGrupoAlimentar() {
		return grupoAlimentar;
	}

	public void setGrupoAlimentar(GrupoAlimentar grupoAlimentar) {
		this.grupoAlimentar = grupoAlimentar;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alimento other = (Alimento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
