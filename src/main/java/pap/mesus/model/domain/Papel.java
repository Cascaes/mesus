package pap.mesus.model.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PAPEL")
public class Papel implements Serializable {
	private static final long serialVersionUID = -1345891543250537278L;
	private static final String TABELA_PAPEL = "PAPEL";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_TIPO = "TIPO";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_PAPEL, name = COLUNA_ID)
	private Integer id;

	private String tipo;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "usuarioPapeis")
	private Set<Usuario> usuarios = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(table = TABELA_PAPEL, name = COLUNA_TIPO, nullable = false)
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Papel other = (Papel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Papel [id=" + id + ", tipo=" + tipo + "]";
	}

}
