package pap.mesus.model.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

// para efetivar cadastro, CPF insere

@Entity
@Table(name = "USUARIO")
@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Usuario implements Serializable {
	private static final long serialVersionUID = -2723425645520721131L;
	private static final String TABELA_USUARIO = "USUARIO";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_NOME = "NOME";
	public static final String COLUNA_EMAIL = "EMAIL";
	private static final String COLUNA_SENHA = "SENHA";
	private static final String COLUNA_ATIVO = "ATIVO";
	private static final String COLUNA_DATA_CADASTRO = "DATA_CADASTRO";
	private static final String DATETIME = "DATETIME";
	private static final String COLUNA_TOKEN_DATA = "TOKEN_DATA";
	private static final String COLUNA_TOKEN_ACESSO = "TOKEN_ACESSO";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_USUARIO, name = COLUNA_ID)
	private Integer id;

	@Column(table = TABELA_USUARIO, name = COLUNA_NOME, nullable = false)
	private String nome;

	@Column(table = TABELA_USUARIO, name = COLUNA_EMAIL, nullable = false)
	private String email;

	@Column(table = TABELA_USUARIO, name = COLUNA_SENHA, nullable = false)
	private String senha;

	@Column(table = TABELA_USUARIO, name = COLUNA_TOKEN_ACESSO, nullable = true)
	private String tokenAcesso;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(table = TABELA_USUARIO, name = COLUNA_TOKEN_DATA, nullable = true, columnDefinition = DATETIME, unique = true)
	private Date tokenData;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "RELACAO_USUARIO_PAPEL", joinColumns = {
			@JoinColumn(name = "USUARIO_ID") }, inverseJoinColumns = { @JoinColumn(name = "PAPEL_ID") })
	private Set<Papel> usuarioPapeis = new HashSet<>();

	@Column(table = TABELA_USUARIO, name = COLUNA_ATIVO, nullable = false)
	private Boolean ativo = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(table = TABELA_USUARIO, name = COLUNA_DATA_CADASTRO, nullable = false, columnDefinition = DATETIME)
	private Date dataCadastro;

	@Transient
	public String primeiroNome() {
		String[] nomes = getNome().split(" ");
		return nomes[0];
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Set<Papel> getUsuarioPapeis() {
		return usuarioPapeis;
	}

	public void setUsuarioPapeis(Set<Papel> usuarioPapeis) {
		this.usuarioPapeis = usuarioPapeis;
	}

	public String getTokenAcesso() {
		return tokenAcesso;
	}

	public void setTokenAcesso(String tokenAcesso) {
		this.tokenAcesso = tokenAcesso;
	}

	public Date getTokenData() {
		return tokenData;
	}

	public void setTokenData(Date tokenData) {
		this.tokenData = tokenData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nome=" + nome + ", email=" + email + ", senha=" + senha + ", tokenAcesso="
				+ tokenAcesso + ", tokenData=" + tokenData + ", usuarioPapeis=" + usuarioPapeis + ", ativo=" + ativo
				+ ", dataCadastro=" + dataCadastro + "]";
	}

}
