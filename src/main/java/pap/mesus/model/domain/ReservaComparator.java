package pap.mesus.model.domain;

import java.util.Comparator;

public class ReservaComparator implements Comparator<Reserva> {

	@Override
	public int compare(Reserva arg0, Reserva arg1) {

		return arg0.getId().compareTo(arg1.getId());
	}

}
