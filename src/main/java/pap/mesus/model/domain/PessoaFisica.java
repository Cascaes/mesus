package pap.mesus.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PESSOA_FISICA")
public class PessoaFisica implements Serializable {

	private static final long serialVersionUID = 4652256449469496013L;
	private static final String TABELA_PESSOA_FISICA = "PESSOA_FISICA";
	private static final String COLUNA_ID = "ID";
	public static final String COLUNA_CPF = "CPF";
	private static final String COLUNA_TELEFONE = "TELEFONE";
	private static final String COLUNA_DATA_NASCIMENTO = "DATA_NASCIMENTO";
	private static final String COLUNA_GENERO = "GENERO";
	private static final String COLUNA_ENDERECO = "ENDERECO_ID";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_PESSOA_FISICA, name = COLUNA_ID)
	private Integer id;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_ENDERECO, nullable = false, insertable = true, updatable = true)
	private Endereco endereco;

	@Column(table = TABELA_PESSOA_FISICA, name = COLUNA_CPF, nullable = false)
	private String cpf;

	@Column(table = TABELA_PESSOA_FISICA, name = COLUNA_TELEFONE, nullable = false)
	private String telefone;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	private Usuario usuario;

	@Temporal(TemporalType.DATE)
	@Column(table = TABELA_PESSOA_FISICA, name = COLUNA_DATA_NASCIMENTO, nullable = false)
	private Date dataNascimento;

	@Column(table = TABELA_PESSOA_FISICA, name = COLUNA_GENERO, nullable = false)
	private String genero;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
