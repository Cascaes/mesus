package pap.mesus.model.domain.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DADOS_DOACOES")
public class DadosDoacoes implements Serializable {

	private static final long serialVersionUID = 9218962339297141105L;

	@Id
	@Column(name = "ID_DOACAO")
	private Integer idDoacao;

	@Id
	@Column(name = "ID_USUARIO")
	private Integer idUsuario;

	@Column(name = "GRAMAS")
	private Integer gramas;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "GRUPO_ALIMENTAR")
	private String grupoAlimentar;

	@Column(name = "ABERTO")
	private boolean aberto;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "LATITUDE")
	private double latitude;

	@Column(name = "LONGITUDE")
	private double longitude;

	public Integer getIdDoacao() {
		return idDoacao;
	}

	public void setIdDoacao(Integer idDoacao) {
		this.idDoacao = idDoacao;
	}

	public Integer getGramas() {
		return gramas;
	}

	public void setGramas(Integer gramas) {
		this.gramas = gramas;
	}

	public String getGrupoAlimentar() {
		return grupoAlimentar;
	}

	public void setGrupoAlimentar(String grupoAlimentar) {
		this.grupoAlimentar = grupoAlimentar;
	}

	public boolean isAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "DadosDoacoes [idDoacao=" + idDoacao + ", idUsuario=" + idUsuario + ", gramas=" + gramas + ", nome="
				+ nome + ", grupoAlimentar=" + grupoAlimentar + ", aberto=" + aberto + ", status=" + status
				+ ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

}
