package pap.mesus.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RECUPERACAO_SENHA")
public class RecuperacaoSenha implements Serializable {
	private static final long serialVersionUID = 2080763726984129043L;

	private static final String TABELA_RECUPERACAO_SENHA = "RECUPERACAO_SENHA";

	private static final String COLUNA_ID = "ID";

	private static final String COLUNA_TOKEN = "TOKEN";

	private static final String COLUNA_DATA_EXPIRACAO = "DATA_EXPIRACAO";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_RECUPERACAO_SENHA, name = COLUNA_ID)
	private Long id;

	@Column(table = TABELA_RECUPERACAO_SENHA, name = COLUNA_TOKEN)
	private String token;

	@OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "USUARIO_ID")
	private Usuario usuario;

	@Column(table = TABELA_RECUPERACAO_SENHA, name = COLUNA_DATA_EXPIRACAO)
	private Date dataExpiracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}

}
