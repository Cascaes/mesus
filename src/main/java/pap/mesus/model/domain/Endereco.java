package pap.mesus.model.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ENDERECO")
public class Endereco implements Serializable {

	private static final long serialVersionUID = 352602342693914047L;
	private static final String TABELA_ENDERECO = "ENDERECO";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_LOGRADOURO = "LOGRADOURO";
	private static final String COLUNA_BAIRRO = "BAIRRO";
	private static final String COLUNA_NUMERO = "NUMERO";
	private static final String COLUNA_CIDADE = "CIDADE";
	private static final String COLUNA_ESTADO = "ESTADO";
	private static final String COLUNA_CEP = "CEP";
	private static final String COLUNA_COMPLEMENTO = "COMPLEMENTO";
	private static final String COLUNA_LATITUDE = "LATITUDE";
	private static final String COLUNA_LONGITUDE = "LONGITUDE";
	private static final String ENDERECO_MAPPED = "endereco";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_ENDERECO, name = COLUNA_ID)
	private Integer id;

	private String logradouro;
	private String bairro;
	private String numero;
	private String cidade;
	private String estado;
	private String CEP;
	private String complemento;
	private Double latitude;
	private Double longitude;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = ENDERECO_MAPPED)
	private PessoaFisica pessoaFisica;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = ENDERECO_MAPPED)
	private PessoaJuridica pessoaJuridica;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_LOGRADOURO, nullable = false)
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_BAIRRO, nullable = true)
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_NUMERO, nullable = true)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_CIDADE, nullable = false)
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_ESTADO, nullable = false)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_CEP, nullable = false)
	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_COMPLEMENTO, nullable = true)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_LATITUDE, nullable = false)
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Column(table = TABELA_ENDERECO, name = COLUNA_LONGITUDE, nullable = false)
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CEP == null) ? 0 : CEP.hashCode());
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((complemento == null) ? 0 : complemento.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((logradouro == null) ? 0 : logradouro.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (CEP == null) {
			if (other.CEP != null)
				return false;
		} else if (!CEP.equals(other.CEP))
			return false;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (complemento == null) {
			if (other.complemento != null)
				return false;
		} else if (!complemento.equals(other.complemento))
			return false;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (logradouro == null) {
			if (other.logradouro != null)
				return false;
		} else if (!logradouro.equals(other.logradouro))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Endereco [id=" + id + ", logradouro=" + logradouro + ", bairro=" + bairro + ", numero=" + numero
				+ ", cidade=" + cidade + ", estado=" + estado + ", CEP=" + CEP + ", complemento=" + complemento
				+ ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

}
