package pap.mesus.model.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/*
 * http://www.chalu.com.br/Docs/Chalu_FichaCadastralPessoaJuridica.pdf
 * http://www.bb.com.br/portalbb/frm/fw0701413_1.jsp
 * http://www.ribeiroimoveis.com.br/docs/cadastro_pessoa_juridica.pdf
 * http://www.solidez.com.br/ficha_pessoa_juridica.pdf
 * 
 * */

@Entity
@Table(name = "PESSOA_JURIDICA")
public class PessoaJuridica implements Serializable {
	private static final long serialVersionUID = 4522921463618160460L;
	private static final String TABELA_PESSOA_JURIDICA = "PESSOA_JURIDICA";
	private static final String COLUNA_ID = "ID";
	public static final String COLUNA_CNPJ = "CNPJ";
	private static final String COLUNA_TELEFONE = "TELEFONE";
	private static final String COLUNA_NOME = "NOME";
	private static final String COLUNA_NOME_FANTASIA = "NOME_FANTASIA";
	public static final String COLUNA_USUARIO = "USUARIO_ID";
	private static final String COLUNA_ENDERECO = "ENDERECO_ID";
	private static final String COLUNA_TOKEN = "TOKEN";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_ID)
	private Integer id;
	private String cnpj;

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_NOME_FANTASIA, nullable = true)
	private String nomeFantasia;

	private String nome;

	private String token;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_USUARIO, nullable = false, insertable = true, updatable = true)
	private Usuario usuario;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_ENDERECO, nullable = false, insertable = true, updatable = true)
	private Endereco endereco;

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_TELEFONE, nullable = false)
	private String telefone;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_CNPJ, nullable = false)
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_USUARIO, nullable = false)
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_ENDERECO, nullable = false)
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_NOME, nullable = false)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Column(table = TABELA_PESSOA_JURIDICA, name = COLUNA_TOKEN, nullable = true)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [id=" + id + ", cnpj=" + cnpj + ", nomeFantasia=" + nomeFantasia + ", nome=" + nome
				+ ", token=" + token + ", usuario=" + usuario + ", endereco=" + endereco + ", telefone=" + telefone
				+ "]";
	}

}
