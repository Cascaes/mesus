package pap.mesus.model.domain;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import pap.mesus.model.domain.abstracts.CipherId;

@Entity
@Table(name = "DOACAO")
@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Doacao extends CipherId implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String TABELA_DOACAO = "DOACAO";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_GRAMAS = "GRAMAS";
	private static final String COLUNA_DATA = "DATA";
	private static final String DATE = "DATE";
	private static final String COLUNA_USUARIO = "USUARIO_ID";
	private static final String COLUNA_GRUPO_ALIMENTAR = "GRUPO_ALIMENTAR_ID";
	public static final String COLUNA_STATUS = "STATUS_ID";
	private static final String COLUNA_IDENTIFICADOR = "IDENTIFICADOR";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_DOACAO, name = COLUNA_ID)
	private Integer id;

	@Column(table = TABELA_DOACAO, name = COLUNA_GRAMAS, nullable = false)
	private Integer gramas;

	@Column(table = TABELA_DOACAO, name = COLUNA_IDENTIFICADOR, nullable = false, unique = true)
	private Long identificador;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(table = TABELA_DOACAO, name = COLUNA_DATA, nullable = false, columnDefinition = DATE)
	private Date data;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_USUARIO, nullable = false, insertable = true, updatable = true)
	private Usuario usuario;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_GRUPO_ALIMENTAR, nullable = false, insertable = true, updatable = true)
	private GrupoAlimentar grupoAlimentar;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = COLUNA_STATUS, nullable = false, insertable = true, updatable = true)
	private Status status;

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGramas() {
		return gramas;
	}

	public void setGramas(Integer gramas) {
		this.gramas = gramas;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Transient
	public String getDataFormatada() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(getData());
	}

	@Transient
	public int getPrazoDias() {
		Calendar agora = Calendar.getInstance();
		Calendar e = Calendar.getInstance();
		e.setTime(getData());
		e.add(Calendar.DAY_OF_MONTH, getGrupoAlimentar().getValidade());
		int r = (int) ((e.getTimeInMillis() - agora.getTimeInMillis()) / (3600000 * 24));
		return r >= 0 ? r + 1 : 0;
	}

	@Transient
	public String getPesoKg() {
		Double p = (double) (getGramas() / 1000);
		Locale ptBr = new Locale("pt", "BR");
		NumberFormat nf = NumberFormat.getNumberInstance(ptBr);
		nf.setGroupingUsed(true);
		return nf.format(p);
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public GrupoAlimentar getGrupoAlimentar() {
		return grupoAlimentar;
	}

	public void setGrupoAlimentar(GrupoAlimentar grupoAlimentar) {
		this.grupoAlimentar = grupoAlimentar;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doacao other = (Doacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
