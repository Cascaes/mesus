package pap.mesus.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

@Entity
@Table(name = "STATUS")
@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Status implements Serializable {
	private static final long serialVersionUID = 2383132579023705926L;
	private static final String TABELA_STATUS = "STATUS";
	private static final String COLUNA_ID = "ID";
	private static final String COLUNA_TITULO = "TITULO";
	private static final String COLUNA_BLOQUEIO = "BLOQUEIO";
	private static final String COLUNA_ROTULO = "ROTULO";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(table = TABELA_STATUS, name = COLUNA_ID)
	public Integer id;

	@Column(table = TABELA_STATUS, name = COLUNA_TITULO, nullable = false)
	public String titulo;

	@Column(table = TABELA_STATUS, name = COLUNA_BLOQUEIO, nullable = false)
	public boolean bloqueio;

	@Column(table = TABELA_STATUS, name = COLUNA_ROTULO, nullable = false)
	public String rotulo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(boolean bloqueio) {
		this.bloqueio = bloqueio;
	}

	public String getRotulo() {
		return rotulo;
	}

	public void setRotulo(String rotulo) {
		this.rotulo = rotulo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Status other = (Status) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
