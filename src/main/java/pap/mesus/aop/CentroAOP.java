package pap.mesus.aop;

import java.util.List;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pap.mesus.controller.service.IDoacaoService;
import pap.mesus.controller.service.IService;
import pap.mesus.controller.service.IStatusService;
import pap.mesus.controller.utils.GoogleMapsMesus;
import pap.mesus.model.domain.Doacao;
import pap.mesus.model.domain.Endereco;
import pap.mesus.model.domain.Status;

@Aspect
@Component
public class CentroAOP {

	@Autowired
	private IService<Endereco> enderecoService;

	@Autowired
	private IDoacaoService doacaoService;

	@Autowired
	private IStatusService statusService;

	@Before("execution(* pap.mesus.controller.ReceberDoacaoController.dadosMapa(..))")
	@After("execution(* pap.mesus.controller.CadastroController.cadastroUsuario(..))")
	public void checarLatLon() {
		List<Endereco> enderecos = enderecoService.listar();
		for (Endereco e : enderecos) {
			if (e.getLatitude() == null || e.getLongitude() == null) {
				GoogleMapsMesus.latLngEndereco(e);
				enderecoService.atualizar(e);
			}
		}
	}

	@Before("execution(* pap.mesus.controller.PrincipalController.iframeListarDoacao(..)) || execution(* pap.mesus.controller.CrontabController.script(..))")
	public void atualizarStatus() {
		List<Doacao> doacoes = doacaoService.listar();
		Status expirou = statusService.expirado();
		for (Doacao d : doacoes) {
			if (d.getPrazoDias() == 0) {
				d.setStatus(expirou);
				doacaoService.atualizar(d);
			}
		}
	}

}
